package it.trenkwalderitalia.trenkpluslib;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.google.common.io.ByteStreams;
import com.google.common.io.CharStreams;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.net.CookieManager;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Date;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.trenkwalderitalia.models.TWRispostaBaseModel;
import it.trenkwalderitalia.models.request.TWRichiestaErrorReportModel;
import it.trenkwalderitalia.models.response.TWRispostaDownloadModel;
import it.trenkwalderitalia.trenkpluslib.log.LogcatLogger;
import it.trenkwalderitalia.trenkpluslib.log.TrenkPlusLogger;
import it.trenkwalderitalia.trenkpluslib.utils.JsonDateDeserializer;

/**
 * Rappresenta un client a TrenkPlus.
 * E' in grado di fare richieste a tutti i metodi di  Trenk+ in modo sincrono o asincrono.
 * <pre>Esempi:</pre>
 * <pre>
 * {@code
 *  new TrenkPlusClient(this, this, 0, logLevel).execute(trkpConnection);
 *
 *  new TrenkPlusClient(this, this, 0, logLevel).execute(new TrenkPlusConnectionBuilder()
 *      .setMethod(TrenkPlusConnection.Method.GET)
 *      .setRelativeUrl("/SmartApp/SmartApp/GetMansioniTotali")
 *      .build());
 * }
 * <pre>
 * </pre>
 */
public class TrenkPlusClient extends AsyncTask<TrenkPlusConnection,Void, TrenkPlusClient.Response> {

    //region Interfaccia Handler
    /**
     * Interfaccia mediante la quale un TrenkPlusClient riesce a trasmettere
     * la risposta ricevuta dal server ad un ascoltatore
     */
    public interface Handler {
        void onTrenkPlusResponse(Response response) throws IOException;
    }

    /**
     * Incapsula una risposta di una chiamata a trenk plus.
     * Contiene le informazioni della risposta e il tag associato al task
     */
    @SuppressWarnings("unused")
    public class Response {
        public TWRispostaBaseModel response;
        public int tag;
        public Dictionary<String,String> contextValues;

        public Response(){ }
        public Response(TWRispostaBaseModel response){
            this(response, 0, null);
        }
        public Response(TWRispostaBaseModel response, int tag){
            this(response, tag, null);
        }
        public Response(TWRispostaBaseModel response, int tag, Dictionary<String,String> contextValues){
            this.response = response;
            this.tag = tag;
            this.contextValues = contextValues;
        }

        public boolean isOk(){
            return TWRispostaBaseModel.isOk(response);
        }
    }

    /** Enumeratore dei livelli di log */
    public enum LogLevel{
        None,
        Essential,
        Complete
    }
    //endregion


    //region Costanti
    public static final String URL_TRENKWALDERITALIA = "https://www.trenkwalderitalia.it";

    /**
     * Url di default di Trenk+ nel caso in cui non ne venga specificato uno.
     * NON MODIFICARE MAI QUESTO VALORE, è un paracadute.
     * per avere un indirizzo personalizzato passarlo opportunamente a {@link #registerForApplication}
     */
    public static final String BASE_URL_DEFAULT = "https://www.trenkwalderitalia.it:8443/trkp";

    /** Imposta il tempo di timeout di una richiesta */
    public static final int CONNECTION_TIMEOUT = 25000; /* millisecondi: 25 secondi */

    /** Imposta il tempo di timeout di lettura di una risposta */
    public static final int CONNECTION_READ_TIMEOUT = 15000; /* millisecondi: 15 secondi */

    /** Tag per i log nel logcat */
    private static final String TAG = "TrenkPlusClient";


    //region Costanti per Multipart
    private static final String CRLF = "\r\n";
    private static final String HYPENS = "--";
    private static final String BOUNDARY =  "*****";
    private static final String CONTENT_TYPE_MULTIPART = "multipart/form-data;boundary=" + BOUNDARY;
    private static final String DEFAULT_FILE_NAME_REMOTE = "file";
    //endregion
    //endregion


    //region Campi statici
    /** Contiene il valore effettivo dell'url base per le chiamate */
    private static String BASE_URL;

    /** Contiene il valore dello user agent inviato durante tutte le chiamate HTTP */
    public static String UserAgent = null;

    /**
     * Contiene il livello di log da utilizzare di default, qualora non ne fosse specificato
     * uno in particolare in una istanza specifica.
     */
    private static LogLevel DefaultLogLevel = LogLevel.None;
    //endregion


    //region Campi
    /** Contiene un riferimento al contesto nel quale è stato istanziato */
    private Context mRequestContext;

    /** Contiene un riferimento all'handler che gestirà la risposta */
    private Handler mResponseHandler;

    /** Contiene il tag assegnato a questa istanza (info logica dell'handler) */
    private int mTaskTag;

    /** Contiene il livello di log utilizzato per tracciare nel logcat */
    private LogLevel mLogLevel;

    /** Contiene eventuali valori passati dal contesto, da ritornare per fare operazioni post-execute */
    private Dictionary<String, String> mContextValues;

    /** Memorizza la risposta grezza ricevuta dal server */
    private String mRawResponse;

    /** Indica se la richiesta è in corso */
    private boolean isRunning = false;

    /** Gestisce i cookie inviati dal server */
    private static CookieManager sCookieManager = new CookieManager();

    /**
     * quando si esegue {@link #requestSyncronously} indica se notificare l'handler
     * come nelle esecuzioni tradizionali del task oppure no.
     */
    private boolean mAsyncCallback = true;
    //endregion


    //region Costruttori e inizializzatori
    /**
     * Registra i parametri fondamentali per permettere all'applicazione di presentarsi a Trenk+
     * @param overrideBaseUrl Url radice al quale è raggiungibile Trenk+
     * @param appId id della applicazione
     * @param appVersion versione della applicazione
     * @param logLevel livello di log da utilizzare nei punti salienti
     */
    public static void registerForApplication(String overrideBaseUrl, int appId, String appVersion, LogLevel logLevel){
        // imposta il base url
        BASE_URL = !TextUtils.isEmpty(overrideBaseUrl) ? overrideBaseUrl : BASE_URL_DEFAULT;

        // imposta lo user agent che sarà usato in tutte le chiamate
        UserAgent = String.format("TWSMARTAPP (APPID=%d;Android;APPVER=%s;%s)",appId,appVersion,Build.VERSION.RELEASE);

        // imposta il livello di log
        DefaultLogLevel = logLevel;

        // crea le extra info da agganciare ad ogni log di una eccezione su trenk+
        TrenkPlusLogger.DefaultLogExtraInfo = buildExceptionLogExtraInfo(appId, appVersion);
    }

    /**
     * Istanzia un nuovo client per effettuare una richiesta a Trenk+
     * @param context il contesto nel quale si esegue la richiesta
     * @param handler il gestore della risposta
     */
    public TrenkPlusClient(Context context, Handler handler) {
        this(context, handler, 0, DefaultLogLevel);
    }

    /**
     * Istanzia un nuovo client per effettuare una richiesta a Trenk+
     * @param context il contesto nel quale si esegue la richiesta
     * @param handler il gestore della risposta
     * @param taskTag un tag che identifica la richiesta corrente (utile per l'handler per capire
     *                di che risposta si tratta {@link #onPostExecute(Response)} )
     * @param logLevel livello di log
     */
    public TrenkPlusClient(Context context, Handler handler, int taskTag, LogLevel logLevel) {
        mRequestContext = context;
        mResponseHandler = handler;
        mTaskTag = taskTag;
        mLogLevel = logLevel;
    }

    /**
     * Istanzia un nuovo client per effettuare una richiesta a Trenk+
     * @param context il contesto nel quale si esegue la richiesta
     * @param handler il gestore della risposta
     * @param taskTag un tag che identifica la richiesta corrente (utile per l'handler per capire
     *                di che risposta si tratta {@link #onPostExecute(Response)} )
     */
    public TrenkPlusClient(Context context, Handler handler, int taskTag) {
        mRequestContext = context;
        mResponseHandler = handler;
        mTaskTag = taskTag;
        mLogLevel = DefaultLogLevel;
    }


    public TrenkPlusClient(Context context, Handler handler, int taskTag, Dictionary<String, String> contextValues) {
        mRequestContext = context;
        mResponseHandler = handler;
        mTaskTag = taskTag;
        mLogLevel = DefaultLogLevel;
        mContextValues = contextValues;
    }
    //endregion


    //region Metodi statici privati
    /**
     * Crea la mappa di informazioni da inviare ad ogni log di una eccezione su Trenk+
     * @param appId id applicazione
     * @param appVersion versione applicazione
     * @return la mappa di extrainfo
     */
    private static @NotNull Map<String,Object> buildExceptionLogExtraInfo(int appId, String appVersion){
        HashMap<String,Object> extraInfo = new HashMap<>();
        extraInfo.put("IdApplication", appId);
        extraInfo.put("AppVersion", appVersion);
        extraInfo.putAll(getSystemExtraInfo());
        return extraInfo;
    }

    /**
     * Crea la mappa di informazioni di sistema da inviare ad ogni log di una eccezione su Trenk+
     * @return la mappa di extrainfo
     */
    private static @NotNull Map<String,Object> getSystemExtraInfo(){
        Gson gson = new Gson();
        JsonObject jo = new JsonObject();
        jo.add("Release", gson.toJsonTree(Build.VERSION.RELEASE));
        jo.add("SDKInt", gson.toJsonTree(Build.VERSION.SDK_INT));
        jo.add("Codename", gson.toJsonTree(Build.VERSION.CODENAME));
        jo.add("Incremental", gson.toJsonTree(Build.VERSION.INCREMENTAL));

        HashMap<String,Object> sysExtraInfo = new HashMap<>();
        sysExtraInfo.put("Board", Build.BOARD);
        sysExtraInfo.put("Bootloader", Build.BOOTLOADER);
        sysExtraInfo.put("Brand", Build.BRAND);
        sysExtraInfo.put("Device", Build.DEVICE);
        sysExtraInfo.put("Display", Build.DISPLAY);
        sysExtraInfo.put("Fingerprint", Build.FINGERPRINT);
        sysExtraInfo.put("Hardware", Build.HARDWARE);
        sysExtraInfo.put("Host", Build.HOST);
        sysExtraInfo.put("ID", Build.ID);
        sysExtraInfo.put("Manufacturer", Build.MANUFACTURER);
        sysExtraInfo.put("Model", Build.MODEL);
        sysExtraInfo.put("Product", Build.PRODUCT);
        sysExtraInfo.put("Serial", Build.SERIAL);
        sysExtraInfo.put("Tags", Build.TAGS);
        sysExtraInfo.put("Time", Build.TIME);
        sysExtraInfo.put("Unknown", Build.UNKNOWN);
        sysExtraInfo.put("Types", Build.TYPE);
        sysExtraInfo.put("User", Build.USER);
        sysExtraInfo.put("Version", jo);
        return sysExtraInfo;
    }
    //endregion


    //region Callback AsyncTask
    @Override
    protected TrenkPlusClient.Response doInBackground(TrenkPlusConnection... params) {
        if(UserAgent == null){
            Log.e(TAG,"TrenkPlusClient non registrato: richiamare all'avvio registerForApplication.");
            return null;
        }

        // verifica se il device è connesso a internet
        if(!isNetworkConnected())
            return new Response(TWRispostaBaseModel.newError(mRequestContext.getString(R.string.device_offline)), mTaskTag, mContextValues);

        TWRispostaBaseModel response = null;
        if(!isCancelled()) {
            // non è stato cancellato il task, estrae le info per fare la richiesta
            TrenkPlusConnection trkpConn = params[0];

            // esegue la richiesta
            response = executeRequest(trkpConn);
        }

        // ritorna la risposta
        return new Response(response, mTaskTag, mContextValues);

        /** dopo questo viene invocato {@link #onPostExecute(Response)} */
    }

    @Override
    protected void onPostExecute(Response response) {
        super.onPostExecute(response);
        if(!isCancelled() && mResponseHandler != null && mAsyncCallback)
            // notifica l'handler se il task non è stato cancellato e la chiamata era asincrona
            try {
                mResponseHandler.onTrenkPlusResponse(response);
            } catch (IOException e) {
                e.printStackTrace();
            }
    }
    //endregion


    //region Metodi pubblici
    /**
     * Avvia una nuova richiesta se il client non sta già facendo un'altra richiesta
     * @param connection informazioni logiche sulla connessione
     * @return l'istanza del task avviato, oppure null se nessun task è stato avviato
     */
    public @Nullable final AsyncTask<TrenkPlusConnection, Void, Response> request(TrenkPlusConnection connection) {
        if(isRunning)
            return null;

        return execute(connection);

        /** dopo questo viene invocato {@link #doInBackground(TrenkPlusConnection[])} */
    }

    /**
     * Effettua la connessione in modo sincrono sul thread corrente, dalla quale viene invocata
     * @param connection informazioni logiche sulla connessione
     * @return la risposta del server
     */
    public TWRispostaBaseModel requestSyncronously(TrenkPlusConnection connection){
        return requestSyncronously(connection, false);
    }

    /**
     * Effettua la connessione in modo sincrono sul thread corrente, dalla quale viene invocata
     * @param connection informazioni logiche sulla connessione
     * @param asyncCallback indica se dopo aver eseguito la richiesta richiamare la callback
     * @return la risposta del server
     */
    public TWRispostaBaseModel requestSyncronously(TrenkPlusConnection connection, boolean asyncCallback){
        mAsyncCallback = asyncCallback;
        return executeRequest(connection);
    }
    //endregion


    //region Metodi privati
    /**
     * Esegue una richiesta HTTP a Trenk+
     * @param trkpConn informazioni sulla connessione
     * @return la risposta
     */
    private @NotNull TWRispostaBaseModel executeRequest(@NotNull TrenkPlusConnection trkpConn){
        isRunning = true;
        TWRispostaBaseModel response = new TWRispostaBaseModel();
        HttpURLConnection connection = null;
        try {
            // instaura la connessione al server
            connection = newConnection(trkpConn);

            if(!isCancelled()) {
                // esegue la richiesta e legge lo stato della risposta
                int statusCode = connection.getResponseCode();
                if (statusCode == HttpURLConnection.HTTP_OK) {
                    // la richiesta è andata a buon fine, interpreta la risposta
                    response = readResponse(connection, trkpConn);

                    if (trkpConn.isLogin() && TWRispostaBaseModel.isOk(response)) {
                        // è appena stato fatto il login, legge i Cookie inviati dal server
                        storeResponseCookies(connection);
                    }
                }
                else {
                    // la richiesta non è andata a buon fine, ritorna un messaggio di errore
                    response = TWRispostaBaseModel.newError(getErrorMessage(statusCode));
                }
            }
        } catch (Exception e){
            // si è verificata un'eccezione
            e.printStackTrace();

            // recupera il messaggio di errore per l'eccezione
            String detailMessage = getExceptionErrorMessage(e);

            // crea la risposta col messaggio d'errore.
            response = TWRispostaBaseModel.newError(getErrorMessage(detailMessage));
        }
        finally {
            if(connection != null)
                connection.disconnect();

            logHttpCall(connection, trkpConn, mRawResponse);
            isRunning = false;
        }
        return response;
    }




    /**
     * Crea un nuovo oggetto capace di effettuare una connessione al server,
     * impostando tutti i parametri definiti
     * @param trkpConn informazioni sulla connessione
     * @return l'oggetto HttpURLConnection per fare la connessione
     * @throws IOException i parametri scelti per la connessione non sono validi
     */
    private HttpURLConnection newConnection(@NotNull TrenkPlusConnection trkpConn) throws IOException{
        HttpURLConnection connection = null;
        try{
            // crea l'url finale
            URL url = new URL(BASE_URL + trkpConn.toString());

            // imposta i parametri della connessione
            connection = (HttpURLConnection) url.openConnection();

            connection.setReadTimeout(CONNECTION_READ_TIMEOUT);
            connection.setConnectTimeout(CONNECTION_TIMEOUT);
            connection.setRequestProperty("User-Agent", UserAgent);
            if(trkpConn.isAuthenticated)
                // inserisce nella request i cookie passati in precedenza dal server
                connection.setRequestProperty("Cookie", TextUtils.join(";", sCookieManager.getCookieStore().getCookies()));

            // ci si aspetta sempre una risposta
            connection.setDoInput(true);

            connection.setRequestMethod(trkpConn.getMethodString());
            if(trkpConn.hasFile()){
                writeUploadToRequest(connection, trkpConn);
            }
            else if(trkpConn.method == TrenkPlusConnection.Method.POST && trkpConn.hasParameters()) {
                connection.setDoOutput(true);

                // scrive la querystring dei parametri nel corpo della richiesta, secondo le specifiche del POST
                writeToRequestStream(connection, trkpConn.getParametersQueryString());
            }
        } catch (Exception e){
            e.printStackTrace();
            if(connection != null)
                connection.disconnect();
        }
        return connection;
    }



    /**
     * Scrive nella request i byte di un file da uploadare.
     * Nota: non funziona come multipart, ma solo come upload di un file, senza altri parametri!
     * @param connection la connessione
     * @param trkpConn info logiche sulla connessione
     * @throws IOException
     */
    private void writeUploadToRequest(@NotNull HttpURLConnection connection, @NotNull TrenkPlusConnection trkpConn) throws IOException{
        if(!trkpConn.file.canRead())
            throw new IOException("Impossibile inviare il file: file non accessibile");

        // imposta la richiesta come multipart
        connection.setRequestProperty("Connection", "Keep-Alive");
        connection.setRequestProperty("Cache-Control", "no-cache");
        connection.setRequestProperty("Content-Type", CONTENT_TYPE_MULTIPART);
        connection.setChunkedStreamingMode(1024);

        // recupera il nome del file da comunicare al server (lo stesso definito nella action)
        String fileNameRemote = getFileNameRemoteOrDefault(trkpConn);

        // recupera i byte del file
//        byte[] fileBytes = ByteStreams.toByteArray(new FileInputStream(trkpConn.file));

        // scrive il contenuto della richiesta
        DataOutputStream request = new DataOutputStream(connection.getOutputStream());
        request.writeBytes(HYPENS + BOUNDARY + CRLF); /* header */
        request.writeBytes(String.format("Content-Disposition: form-data; name=\"%s\";filename=\"%s\"%s", fileNameRemote, trkpConn.file.getName(), CRLF));
        request.writeBytes(CRLF);
        chunkUpload(request,trkpConn);
//        request.write(fileBytes);   /* body */
        request.writeBytes(CRLF);   /* footer */
        request.writeBytes(HYPENS + BOUNDARY + HYPENS + CRLF);

        request.flush();
        request.close();
    }

    private void chunkUpload(DataOutputStream request, TrenkPlusConnection trkpConn)  throws IOException {
        // recupera i byte del file

        RandomAccessFile raf = new RandomAccessFile( trkpConn.file, "rw" ) ;
        FileChannel fc = raf.getChannel();
        MappedByteBuffer mappedByteBuffer = fc.map(FileChannel.MapMode.READ_WRITE, 0, (int) fc.size());

        byte[] data = new byte[1024];  // Needs to be Byte Array only as MappedBuffer play only with Byte[]
        while (mappedByteBuffer.hasRemaining()) {
            int remaining = Math.min(data.length, mappedByteBuffer.remaining());
//            ByteBuffer buffer = mappedByteBuffer.get(data, 0, remaining);
            // do somthing with data

           // Log.d("TrenkPlusClinet", "data: " + fc.read(buffer));
            try{
                byte[] temp = new byte[remaining];
                mappedByteBuffer.get(temp, 0, temp.length);
                request.write(temp);
            }catch(Exception e){
                e.printStackTrace();
            }

        }
    }

    /**
     * {@link TrenkPlusConnection#fileNameRemote} o il nome di default
     * @param trkpConn info logiche sulla connessione
     * @return il nome del file finale
     */
    private String getFileNameRemoteOrDefault(@NotNull TrenkPlusConnection trkpConn){
        String fileNameRemote = trkpConn.fileNameRemote;
        if (TextUtils.isEmpty(fileNameRemote)) {
            Log.w(TAG, "Nome del file (fileNameRemote) non definito. Il nome del file inviato sarà: " + DEFAULT_FILE_NAME_REMOTE);
            fileNameRemote = DEFAULT_FILE_NAME_REMOTE;
        }
        return fileNameRemote;
    }

    private void writeToRequestStream(HttpURLConnection connection, String... contents) throws IOException{
        if(connection != null && contents != null){
            OutputStream os = connection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            for(String value : contents) {
                writer.write(value);
            }
            writer.flush();
            writer.close();
            os.close();
        }
    }

    /**
     * Indica se il device è connesso alla rete.
     * @return true se è connesso o non è stato in grado di capirlo
     * (probabilmente perchè {@link #mRequestContext} è <code>null</code>)
     */
    private boolean isNetworkConnected(){
        Boolean isConnected = isNetworkConnectedOrDefault();
        // se è null non è stato in grado di capirlo
        return isConnected == null || isConnected;
    }

    /**
     * Controlla se il device è connesso alla rete
     * @return è connesso SI/NO o null se non è stato in grado di capirlo.
     */
    @Nullable
    private Boolean isNetworkConnectedOrDefault(){
        try {
            if(mRequestContext != null) {
                ConnectivityManager connMgr = (ConnectivityManager) mRequestContext.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
                return (networkInfo != null && networkInfo.isConnected());
            }
        } catch(Exception e){
            e.printStackTrace();
        }

        // non è stato in grado di capire se è connesso o no
        return null;
    }

    /**
     * Salva nel cookie store dell'applicazione i cookie inviati dal server
     * @param connection la connessione con il server
     */
    private void storeResponseCookies(@NotNull HttpURLConnection connection){
        Map<String, List<String>> headerFields = connection.getHeaderFields();
        List<String> cookiesHeader = headerFields.get("Set-Cookie");

        if(cookiesHeader != null){
            for (String cookie : cookiesHeader){
                sCookieManager.getCookieStore().add(null,HttpCookie.parse(cookie).get(0));
            }
        }
    }
    //endregion


    //region Log Exception

    /**
     * Riporta un eccezione a trenk plus per la diagnostica
     * @param context il contesto nel quale si effettua il report
     * @param throwable l'eccezione software, di sistema o errore da segnalare
     */
    public static void reportError(Context context, Throwable throwable){
        reportError(context, throwable, null);
    }

    /**
     * Riporta un eccezione a trenk plus per la diagnostica
     * @param context il contesto nel quale si effettua il report
     * @param throwable l'eccezione software, di sistema o errore da segnalare
     * @param extraInfo informazioni utili aggiuntive
     */
    public static void reportError(Context context, Throwable throwable, Map<String, Object> extraInfo){
        if(throwable == null)
            return;

        reportError(context, new TWRichiestaErrorReportModel(throwable, extraInfo));
    }

    /**
     * Riporta un eccezione a trenk plus per la diagnostica
     * @param context il contesto nel quale si effettua il report
     * @param error informazioni sull'errore
     */
    public static void reportError(Context context, TWRichiestaErrorReportModel error){
        if(error == null)
            return;

        TrenkPlusLogger.instance.log(context, error);
    }
    //endregion


    //region Errori
    /**
     * Legge e intepreta la risposta del server
     * @param connection la connessione HTTP attiva
     * @param trkpConn le info logiche sulla ocnnessione
     * @param <T> tipo di risposta
     * @return la risposta
     * @throws IOException eccezione che si verifica durante il recupero dello stream della risposta
     * oppure nella deserializzazione
     */
    private <T extends TWRispostaBaseModel> TWRispostaBaseModel readResponse (
            @NotNull HttpURLConnection connection,
            @NotNull TrenkPlusConnection<T> trkpConn) throws IOException
    {
        try {
            String contentType = connection.getHeaderField("Content-Type").toLowerCase();
            if(contentType.startsWith("application/json")) {
                // la risposta è in JSON

                // converte in stringa i byte ricevuti come risposta
                String responseString = CharStreams.toString(new InputStreamReader(connection.getInputStream()));
                mRawResponse = responseString;

                // determina il tipo di output atteso dalla risposta
                // prendendo, se specificato, quello in trkpConn altrimenti la risposta base
                Class<? extends TWRispostaBaseModel> responseClass = trkpConn.responseClass != null
                        ? trkpConn.responseClass
                        : TWRispostaBaseModel.class;

                // converte il Json in un oggetto java da ritornare
                return new GsonBuilder()
                        .registerTypeAdapter(Date.class, new JsonDateDeserializer())
                        .create()
                        .fromJson(responseString, responseClass);
            } else if(contentType.startsWith("application/pdf") || contentType.startsWith("image/") || contentType.startsWith("audio/") || contentType.startsWith("video/")){
                // la risposta è un contenuto multimediale
                byte[] bytes = ByteStreams.toByteArray(connection.getInputStream());
                return new TWRispostaDownloadModel(bytes);
            }
            else {
                // la risposta è di un tipo non gestito
                return TWRispostaBaseModel.newError("Impossibile caricare il contenuto della pagina");
            }

        } catch(IOException e){
            e.printStackTrace();

            // si traccia l'eccezione perchè è causata nel client,
            // e il client riesce a contattare il server
            reportError(mRequestContext, e);

            throw e;
        }
    }


    /**
     * Restituisce il messaggio d'errore da mostrare all'utente relativo ad una tipologia
     * di eccezione che si verifica durante la fase di esecuzione della richiesta.
     * @param e l'eccezione
     * @return il messaggio di errore
     */
    private String getExceptionErrorMessage(Exception e){
        // si è verificata un'eccezione
        String detailMessage = null;
        if(e instanceof IOException)
            detailMessage = "Errore imprevisto durante lo scambio di dati";
        if(e instanceof NullPointerException)
            detailMessage = "Errore interno della applicazione";
        if(e instanceof MalformedURLException)
            detailMessage = "Indirizzo non valido";
        if(e instanceof ProtocolException)
            detailMessage = "Non rispettato il protocollo di scambio di dati con il server";
        if(e instanceof SocketTimeoutException)
            detailMessage = "Tempo di richiesta scaduto, connessione troppo lenta.";
        return detailMessage;
    }

    /**
     * @param responseStatusCode lo status code della risposta ricevuta
     * @return il messaggio d'errore da mostrare all'utente, relativo allo status code
     */
    private String getErrorMessage(int responseStatusCode){
        String detailMessage = null;
        if(responseStatusCode == 401 || responseStatusCode == 407){
            /*
                Errori di non autorizzazione
                http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.2
             */
            detailMessage = "Autenticazione richiesta. Eseguire nuovamente il login per continuare";
        }

        if(responseStatusCode >= 500 && responseStatusCode < 600){
            /*
                Errori interni del server
                http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.5
             */
            detailMessage = "Si è verificato un errore interno nel server";
        }
        return getErrorMessage(detailMessage);
    }

    /**
     * Restituisce il messaggio di errore completo da mostrare
     * (messaggio fisso + messaggio di dettaglio)
     * @param detailMessage messaggio di dettaglio
     * @return il messaggio finale da mostrare all'utente
     */
    private String getErrorMessage(String detailMessage){
        StringBuilder messageBuilder = new StringBuilder();
        messageBuilder.append("Non è stato possibile recuperare i dati dal server");
        if(!TextUtils.isEmpty(detailMessage)) {
            // c'è un messaggio di errore dettagliato
            messageBuilder.append(", per il seguente motivo:\n");
            messageBuilder.append(detailMessage);
        }
        return messageBuilder.toString();
    }
    //endregion


    //region Logcat

    /**
     * @return Indica se il {@link TrenkPlusClient#mLogLevel} è sufficiente per eseguire il log
     */
    private boolean shouldLog(){
        return mLogLevel.ordinal() > LogLevel.None.ordinal();
    }

    /**
     * Fa il log di una chiamata HTTP completa, richiesta e risposta
     * @param connection connessione HTTP
     * @param trkpConn info logiche sulla connessione
     * @param response la risposta
     */
    private void logHttpCall(HttpURLConnection connection, TrenkPlusConnection trkpConn, String response) {
        try{
            logRequest(connection, trkpConn);
            log(" ", false);
            logResponse(connection, trkpConn, response);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Fa il log di una request nel logcat
     * @param connection connessione HTTP
     * @param trenkPlusConnection info logiche sulla connessione
     */
    private void logRequest(HttpURLConnection connection, TrenkPlusConnection trenkPlusConnection){
        if(shouldLog() && connection != null && trenkPlusConnection != null){
            StringBuilder sb = new StringBuilder();
            //es: GET <url>
            sb.append(String.format("%s %s\n", connection.getRequestMethod(), connection.getURL().toString()));

//            if(mLogLevel == LogLevel.Complete){
//                // recupera gli header della richiesta
//                Map<String, List<String>> headers = connection.getRequestProperties();
//                for(String key: headers.keySet()){
//                    // costruisce la riga dell'header corrente (es. Cookie: cookie1,cookie2)
//                    sb.append(String.format("%s: %s\n", key, TextUtils.join(",", headers.get(key))));
//                }
//            }
//            sb.append("\n");

            if(trenkPlusConnection.method == TrenkPlusConnection.Method.POST)
                sb.append(trenkPlusConnection.logParameters());

            log(sb.toString(), false);
        }
    }

    /**
     * Fa il log di una response nel logcat
     * @param connection connessione HTTP
     * @param trenkPlusConnection info logiche sulla connessione
     * @param response la risposta ricevuta
     */
    private void logResponse(HttpURLConnection connection, TrenkPlusConnection trenkPlusConnection, String response){
        if(shouldLog() && connection != null && trenkPlusConnection != null){
            StringBuilder sb = new StringBuilder();

            if(mLogLevel == LogLevel.Complete){
                // recupera gli header della risposta
                Map<String, List<String>> headers = connection.getHeaderFields();
                for(String key: headers.keySet()){
                    String value = TextUtils.join(",", headers.get(key));

                    // costruisce la riga dell'header corrente (es. Set-Cookie: cookie1,cookie2)
                    if(key == null || key.equals("null"))
                        sb.append(String.format("%s\n", value));
                    else
                        sb.append(String.format("%s: %s\n", key, value));
                }
            }
            log(sb.toString(), false);
            log(" ", false);
            log(response);
        }
    }


    /**
     * Fa il log di un messaggio nel logcat,
     * se il {@link TrenkPlusClient#mLogLevel} è sufficiente
     * @param message messaggio da loggare
     */
    private void log(String message){
        log(message, true);
    }

    /**
     * Fa il log di un messaggio nel logcat,
     * se il {@link TrenkPlusClient#mLogLevel} è sufficiente
     * @param message messaggio da loggare
     * @param end indica se il log è finito
     */
    private void log(String message, boolean end){
        if(shouldLog() && message != null) {
            LogcatLogger.instance.log(LogcatLogger.LOG_TAG, message);
            if(end)
                LogcatLogger.instance.log(LogcatLogger.LOG_TAG, "---------------------------------------------------------");
        }
    }
    //endregion


    public static String getAbsoluteUri(String relativeUrl){
        return String.format("%s/%s", BASE_URL, relativeUrl);
    }

    public static String getCookiesHeader(){
        return sCookieManager != null && sCookieManager.getCookieStore() != null
                ? TextUtils.join(";", sCookieManager.getCookieStore().getCookies())
                : "";
    }

    public static List<HttpCookie> getCookies(){
        return sCookieManager.getCookieStore().getCookies();
    }
}
