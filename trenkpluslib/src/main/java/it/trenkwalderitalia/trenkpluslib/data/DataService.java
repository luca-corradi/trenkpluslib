package it.trenkwalderitalia.trenkpluslib.data;

import android.content.Context;
import android.preference.PreferenceManager;

/**
 * Classe incaricata di gestire il recupero/salvataggio dei dati:
 * Settings dell'app, file su filesystem, in memoria, file nell'assets
 */
public class DataService {

    /**
     * Definisce il set completo delle chiavi delle preferenze che si salvano
     */
    public class Keys {
        /** Preferenza che indica se il mobile token è attivato sul dispositivo */
        public static final String MOBILE_TOKEN_ENABLED_KEY = "mobile_token_enabled";

        /** Preferenza che indica se è abilitato l'autologin */
        public static final String AUTOLOGIN_KEY = "autologin";
    }

    //region Preferences

    //region String
    public static String getPreferenceString(Context context, String preferenceGroupName, String key){
        return getPreferenceString(context, preferenceGroupName, key, null);
    }

    public static String getPreferenceString(Context context, String preferenceGroupName, String key, String defaultValue){
        return context.getSharedPreferences(preferenceGroupName, Context.MODE_PRIVATE)
                .getString(key, defaultValue);
    }

    public static String getDefaultPreferenceString(Context context, String key){
        return getDefaultPreferenceString(context, key, null);
    }

    public static String getDefaultPreferenceString(Context context, String key, String defaultValue){
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(key, defaultValue);
    }

    public static boolean savePreferenceString(Context context, String preferenceGroupName, String key, String value){
        return context.getSharedPreferences(preferenceGroupName, Context.MODE_PRIVATE).edit()
                .putString(key, value)
                .commit();
    }

    public static boolean saveDefaultPreferenceString(Context context, String key, String value){
        return PreferenceManager.getDefaultSharedPreferences(context).edit()
                .putString(key, value)
                .commit();
    }
    //endregion

    //region Int
    public static int getPreferenceInt(Context context, String preferenceGroupName, String key){
        return getPreferenceInt(context, preferenceGroupName, key, 0);
    }

    public static int getPreferenceInt(Context context, String preferenceGroupName, String key, int defaultValue){
        return context.getSharedPreferences(preferenceGroupName, Context.MODE_PRIVATE)
                .getInt(key, defaultValue);
    }

    public static int getDefaultPreferenceInt(Context context, String key){
        return getDefaultPreferenceInt(context, key, 0);
    }

    public static int getDefaultPreferenceInt(Context context, String key, int defaultValue){
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getInt(key, defaultValue);
    }

    public static boolean savePreferenceInt(Context context, String preferenceGroupName, String key, int value){
        return context.getSharedPreferences(preferenceGroupName, Context.MODE_PRIVATE).edit()
                .putInt(key, value)
                .commit();
    }

    public static boolean saveDefaultPreferenceInt(Context context, String key, int value){
        return PreferenceManager.getDefaultSharedPreferences(context).edit()
                .putInt(key, value)
                .commit();
    }
    //endregion

    //region Bool
    public static boolean getPreferenceBool(Context context, String preferenceGroupName, String key){
        return getPreferenceBool(context, preferenceGroupName, key, false);
    }

    public static boolean getPreferenceBool(Context context, String preferenceGroupName, String key, boolean defaultValue){
        return context.getSharedPreferences(preferenceGroupName, Context.MODE_PRIVATE)
                .getBoolean(key, defaultValue);
    }

    public static boolean getDefaultPreferenceBool(Context context, String key){
        return getDefaultPreferenceBool(context, key, false);
    }

    public static boolean getDefaultPreferenceBool(Context context, String key, boolean defaultValue){
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(key, defaultValue);
    }

    public static boolean savePreferenceBool(Context context, String preferenceGroupName, String key, boolean value){
        return context.getSharedPreferences(preferenceGroupName, Context.MODE_PRIVATE).edit()
                .putBoolean(key, value)
                .commit();
    }

    public static boolean saveDefaultPreferenceBool(Context context, String key, boolean value){
        return PreferenceManager.getDefaultSharedPreferences(context).edit()
                .putBoolean(key, value)
                .commit();
    }
    //endregion

    //endregion

}

