package it.trenkwalderitalia.trenkpluslib.utils;


import android.os.Bundle;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.trenkwalderitalia.models.util.IgnoreMapping;

public class ReflectionUtils {
    public static Map<String, String> toParameters(Object model){
        if(model != null) {
            try {
                HashMap<String, String> parameters = new HashMap<>();

                // cicla tutti i campi della classe del model
                for(Field field : model.getClass().getDeclaredFields()) {
                    if (!field.isAnnotationPresent(IgnoreMapping.class)) {
                        // si può leggere il campo
                        String name = field.getName();
                        try {
                            Object rawValue = field.get(model);
                            if (rawValue != null) {
                                if (rawValue.getClass().isArray()) {
                                    // il campo è un array
                                    Class componentType = field.getType().getComponentType();
                                    parameters.putAll(toParametersArray((Array) rawValue, componentType, name));
                                } else if (rawValue instanceof List) {
                                    parameters.putAll(toParametersArray((List) rawValue, name));
                                } else if (rawValue instanceof Date) {
                                    //DateFormat dateFormat = SimpleDateFormat.getDateTimeInstance();
                                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    parameters.put(name, dateFormat.format((Date) rawValue));
                                } else {
                                    parameters.put(name, String.valueOf(rawValue));
                                }
                            }
                        } catch (IllegalAccessException ec) {
                        }
                    }
                }
                return parameters;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private static  Map<String, String> toParametersArray(Array array, Class elementType, String name)
            throws IllegalAccessException, InstantiationException {
        HashMap<String, String> params = new HashMap<>();
        if(array != null && elementType != null){
            int i = 0;
            for (Object o : Arrays.asList(array)) {
                if (o != null) {
                    params.put(String.format("%s[%d]", name, i), o.toString());
                    i++;
                }
            }
        }
        return params;
    }

    private static  Map<String, String> toParametersArray(List list, String name)
            throws IllegalAccessException, InstantiationException {
        HashMap<String, String> params = new HashMap<>();
        if(list != null && name != null){
            int i = 0;
            for (Object o : list) {
                if (o != null) {
                    params.put(String.format("%s[%d]", name, i), o.toString());
                    i++;
                }
            }
        }
        return params;
    }

    public static Object toModel(Bundle bundle, Class<?> classe){
        try {
            Object instance = classe.newInstance();
            for (String key: bundle.keySet()){
                Field field = instance.getClass().getDeclaredField(key);
                if(field != null){
                    if(field.getType().isAssignableFrom(Date.class)){
                        long time = bundle.getLong(key + ".Date");
                        field.set(instance, new Date(time));
                    }
                    else field.set(instance, bundle.get(key));
                }
            }
            return instance;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Bundle toBundle(Object model){
        Bundle bundle = new Bundle();

        // cicla tutti i campi della classe del model
        for(Field field : model.getClass().getDeclaredFields()) {
            // si può leggere il campo
            String name = field.getName();
            try {
                Object rawValue = field.get(model);
                if(rawValue instanceof Integer)
                    bundle.putInt(name, (Integer) rawValue);
                else if(rawValue instanceof Boolean)
                    bundle.putBoolean(name, (Boolean) rawValue);
                else if(rawValue instanceof String)
                    bundle.putString(name, (String) rawValue);
                else if(rawValue instanceof Date)
                    bundle.putLong(name + ".Date", ((Date) rawValue).getTime());
                else if(rawValue instanceof Long)
                    bundle.putLong(name, (Long) rawValue);
                else if(rawValue instanceof Double)
                    bundle.putDouble(name, (Double) rawValue);
                else if(rawValue instanceof Character)
                    bundle.putChar(name, (Character) rawValue);
                else if(rawValue instanceof Byte)
                    bundle.putByte(name, (Byte) rawValue);
                else if(rawValue instanceof int[])
                    bundle.putIntArray(name, (int[]) rawValue);
                else if(rawValue instanceof byte[])
                    bundle.putByteArray(name, (byte[]) rawValue);
                else if(rawValue instanceof char[])
                    bundle.putCharArray(name, (char[]) rawValue);
                else if(rawValue instanceof CharSequence)
                    bundle.putCharSequence(name, (CharSequence) rawValue);
                else if (rawValue instanceof CharSequence[])
                    bundle.putCharSequenceArray(name, (CharSequence[]) rawValue);
            } catch (IllegalAccessException ec){
                ec.printStackTrace();
            }
        }

        return bundle;
    }
}
