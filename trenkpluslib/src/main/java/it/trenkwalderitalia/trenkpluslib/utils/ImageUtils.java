package it.trenkwalderitalia.trenkpluslib.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.media.Image;

import org.jetbrains.annotations.Contract;

import java.io.InputStream;

import it.trenkwalderitalia.models.util.ImageInfo;
import it.trenkwalderitalia.trenkpluslib.R;

/**
 * Classe di utility per gestire in modo efficiente le immagini all'interno dell'applicazione
 * @see <a href="http://developer.android.com/training/displaying-bitmaps/load-bitmap.html">
 *     Loading Large Bitmaps Efficiently
 *     </a>
 */
public class ImageUtils {

    //region Bitmap
    /**
     * Crea un Bitmap impostabile in un'immagine, evenutualmente scalando l'immagine originale
     * @param context contesto nel quale si esegue
     * @param resourceId id della risorsa dell'immagine
     * @param desiderWidth larghezza desiderata
     * @param desiredHeight altezza desiderata
     * @return il bitmap
     */
    public static Bitmap decodeBitmapFrom(Context context, int resourceId, Integer desiderWidth, Integer desiredHeight){
        Resources res = context.getResources();

        // Recupera le informazioni sull'immagine senza allocare il bitmap
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resourceId, options);

        if(desiderWidth != null && desiredHeight != null) {
            // Calcola e imposta l'indice di scala
            options.inSampleSize = getScaleIndex(options, desiredHeight, desiderWidth);
        }

        // Decodifica il bitmap con le opzioni impostate
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resourceId, options);
    }

    /**
     * Crea un Bitmap impostabile in un'immagine, evenutualmente scalando l'immagine originale
     * @param filePath percorso del file dell'immagine
     * @param desiderWidth larghezza desiderata
     * @param desiredHeight altezza desiderata
     * @return il bitmap
     */
    public static Bitmap decodeBitmapFrom(String filePath, Integer desiderWidth, Integer desiredHeight){
        // Recupera le informazioni sull'immagine senza allocare il bitmap
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        if(desiderWidth != null && desiredHeight != null) {
            // Calcola e imposta l'indice di scala
            options.inSampleSize = getScaleIndex(options, desiredHeight, desiderWidth);
        }

        // Decodifica il bitmap con le opzioni impostate
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(filePath, options);
    }

    /**
     * Crea un Bitmap impostabile in un'immagine, evenutualmente scalando l'immagine originale
     * @param imageBytes i byte dell'immagine
     * @param desiderWidth larghezza desiderata
     * @param desiredHeight altezza desiderata
     * @return il bitmap
     */
    public static Bitmap decodeBitmapFrom(byte[] imageBytes, Integer desiderWidth, Integer desiredHeight){
        // Recupera le informazioni sull'immagine senza allocare il bitmap
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length, options);

        if(desiderWidth != null && desiredHeight != null) {
            // Calcola e imposta l'indice di scala
            options.inSampleSize = getScaleIndex(options, desiredHeight, desiderWidth);
        }

        // Decodifica il bitmap con le opzioni impostate
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length, options);
    }
    //endreigon

    //region Informazioni immagine
    /**
     * @see <a href="http://developer.android.com/training/displaying-bitmaps/load-bitmap.html#read-bitmap">
     * Read Bitmap Dimensions and Type
     * </a>
     * @param context il contesto nel quale viene usato
     * @param imageResourceId id della risorsa
     * @return le informazioni sull'immagine
     */
    public static ImageInfo getImageInfo(Context context, int imageResourceId){
        BitmapFactory.Options options = new BitmapFactory.Options();

        // impedisce di allocare il bitmap in memoria (ed evitare di ricevere OutOfMemoryException)
        // ma permette di ricevere le informazioni in output
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(context.getResources(), imageResourceId, options);
        return new ImageInfo(options.outHeight, options.outWidth, options.outMimeType);
    }


    /**
     * @see <a href="http://developer.android.com/training/displaying-bitmaps/load-bitmap.html#read-bitmap">
     * Read Bitmap Dimensions and Type
     * </a>
     * @param filePath percorso del file dell'immagine
     * @return le informazioni sull'immagine
     */
    public static ImageInfo getImageInfo(String filePath){
        BitmapFactory.Options options = new BitmapFactory.Options();

        // impedisce di allocare il bitmap in memoria (ed evitare di ricevere OutOfMemoryException)
        // ma permette di ricevere le informazioni in output
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);
        return new ImageInfo(options.outHeight, options.outWidth, options.outMimeType);
    }

    /**
     * @see <a href="http://developer.android.com/training/displaying-bitmaps/load-bitmap.html#read-bitmap">
     * Read Bitmap Dimensions and Type
     * </a>
     * @param imageBytes byte dell'immagine
     * @return le informazioni sull'immagine
     */
    public static ImageInfo getImageInfo(byte[] imageBytes){
        BitmapFactory.Options options = new BitmapFactory.Options();

        // impedisce di allocare il bitmap in memoria (ed evitare di ricevere OutOfMemoryException)
        // ma permette di ricevere le informazioni in output
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length, options);
        return new ImageInfo(options.outHeight, options.outWidth, options.outMimeType);
    }

    /**
     * @see <a href="http://developer.android.com/training/displaying-bitmaps/load-bitmap.html#read-bitmap">
     * Read Bitmap Dimensions and Type
     * </a>
     * @param imageInputStream stream dell'immagine
     * @return le informazioni sull'immagine
     */
    public static ImageInfo getImageInfo(InputStream imageInputStream){
        BitmapFactory.Options options = new BitmapFactory.Options();

        // impedisce di allocare il bitmap in memoria (ed evitare di ricevere OutOfMemoryException)
        // ma permette di ricevere le informazioni in output
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(imageInputStream, new Rect(), options);
        return new ImageInfo(options.outHeight, options.outWidth, options.outMimeType);
    }
    //endregion

    //region Indice di scala
    /**
     * Indica quale indice di scala è necessario per raggiungere le dimensioni desiderate dell'immagine
     * <pre>
     *     Esempio:
     *     2048 x 1536 -> 512 x 384 : Indice = 4
     * </pre>
     * @see <a href="http://developer.android.com/training/displaying-bitmaps/load-bitmap.html#load-bitmap">
     *     Load a Scaled Down Version into Memory
     *     </a>
     * @param imageInfo informazioni sull'immagine
     * @param desiredHeight altezza desiderata
     * @param desiredWidth larghezza desiderata
     * @return l'indice di scala
     */
    public static int getScaleIndex(ImageInfo imageInfo, int desiredHeight, int desiredWidth) {
        return getScaleIndex(imageInfo.getImageHeight(), imageInfo.getImageWidth(), desiredHeight, desiredWidth);
    }

    /**
     * Indica quale indice di scala è necessario per raggiungere le dimensioni desiderate dell'immagine
     * <pre>
     *     Esempio:
     *     2048 x 1536 -> 512 x 384 : Indice = 4
     * </pre>
     * @see <a href="http://developer.android.com/training/displaying-bitmaps/load-bitmap.html#load-bitmap">
     *     Load a Scaled Down Version into Memory
     *     </a>
     * @param options le opzioni di decodifica del bitmap
     * @param desiredHeight altezza desiderata
     * @param desiredWidth larghezza desiderata
     * @return l'indice di scala
     */
    public static int getScaleIndex(BitmapFactory.Options options, int desiredHeight, int desiredWidth) {
        return getScaleIndex(options.outHeight, options.outWidth, desiredHeight, desiredWidth);
    }

    /**
     * Indica quale indice di scala è necessario per raggiungere le dimensioni desiderate dell'immagine
     * <pre>
     *     Esempio:
     *     2048 x 1536 -> 512 x 384 : Indice = 4
     * </pre>
     * @see <a href="http://developer.android.com/training/displaying-bitmaps/load-bitmap.html#load-bitmap">
     *     Load a Scaled Down Version into Memory
     *     </a>
     * @param originalHeight altezza originale
     * @param originalWidth larghezza originale
     * @param desiredHeight altezza desiderata
     * @param desiredWidth larghezza desiderata
     * @return l'indice di scala
     */
    public static int getScaleIndex(int originalHeight, int originalWidth, int desiredHeight, int desiredWidth){
        int inSampleSize = 1;

        if (originalHeight > desiredHeight || originalWidth > desiredWidth) {

            final int halfHeight = originalHeight / 2;
            final int halfWidth = originalWidth / 2;

            // Calcola il valore più alto dell'indice di scala tra le potenze di 2,
            // mantenendo sia altezza che larghezza maggiori delle dimensioni desiderate,
            // per evitare di ricevere una qualità inferiore di quella desiderata
            while ((halfHeight / inSampleSize) > desiredHeight
                    && (halfWidth / inSampleSize) > desiredWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
    //endregion
}
