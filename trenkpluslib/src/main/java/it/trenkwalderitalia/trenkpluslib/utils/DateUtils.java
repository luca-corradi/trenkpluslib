package it.trenkwalderitalia.trenkpluslib.utils;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {

    public static Date addMonths(Date from, int months){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, months);
        return calendar.getTime();
    }

    public static Date addDays(Date from, int days){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, days);
        return calendar.getTime();
    }

    public static Date addYears(Date from, int years){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, years);
        return calendar.getTime();
    }

    public static String toShortDateString(Date date){
        return new SimpleDateFormat("dd/MM/yyyy").format(date);
    }

    public static String toShortTimeString(Date date){
        return toShortTimeString(date, false);
    }

    public static String toShortTimeString(Date date, boolean ignoreSeconds){
        return ignoreSeconds
            ? new SimpleDateFormat("HH:mm").format(date)
            : new SimpleDateFormat("HH:mm:ss").format(date);
    }

    public static String toDateTimeString(Date date){
        return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(date);
    }

    public static String toSmartDateTimeString(String dateString){

        try {
            Date date = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(dateString);
            return toSmartDateTimeString(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return dateString;
        }
    }

    public static String toSmartDateTimeString(Date date){
        String dateString = android.text.format.DateUtils.isToday(date.getTime())
                ? "Oggi"
                : toSmartDateString(date);

        return String.format("%s - %s", dateString, toShortTimeString(date, true));
    }

    public static String toSmartDateString(Date date){
        return new SimpleDateFormat("dd MMMM yyyy").format(date);
    }

    public static String toPeriod(Date date){
        return new SimpleDateFormat("MMMM yyyy").format(date);
    }
}
