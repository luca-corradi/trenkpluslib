package it.trenkwalderitalia.trenkpluslib.utils;

import android.text.TextUtils;

import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import it.trenkwalderitalia.trenkpluslib.log.LogcatLogger;

public class MobileTokenUtils {

    /** Lunghezza del passcode */
    static final int PasscodeLength = 8;


    /**
     * @return un nuovo timestamp da usare per la firma,
     * che tiene conto dello sfasamento con il server
     */
    public static String newTimestamp(int millisecondsOffset){
        // ottiene l'orario attuale del server (orario locale + sfasamento)
        Calendar now = Calendar.getInstance();
        now.add(Calendar.MILLISECOND, millisecondsOffset);

        // imposta i secondi a 0 o 30
        int secondi = now.get(Calendar.SECOND);
        now.set(Calendar.SECOND, secondi > 30 ? 30 : 0);

        // crea e ritorna il timestamp
        DateFormat format = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ITALIAN);
        return format.format(now.getTime());
    }


    /**
     * Genera un nuovo passcode valido per la firma digitale.
     * @param idAgente id agente firmatario
     * @param pin pin dell'agente per il servizio Mobile Token
     * @param userToken chiave segreta inviata dal server
     * @return il passcode
     */
    public static String NewPasscode(int idAgente, String pin, String userToken, int millisecondsOffset)
    {
        // costruisce la Stringa da hashare, concatenando IdAgente, Pin hashato, Timestamp, idAgenteSmartApp
        String concat = TextUtils.concat(String.valueOf(idAgente), HashUtils.SHA1(pin), newTimestamp(millisecondsOffset), userToken).toString();

        return PasscodeWebservice(concat, PasscodeLength);
    }


    public static String PasscodeWebservice(String stringa, int passcodeLength){
        String sResult = "";
        try {
            byte[] bytes = stringa.getBytes("UTF-8");
            MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
            byte[] sha1Bytes = sha1.digest(bytes);
            String sha1String = HashUtils.convertToHex(sha1Bytes);
            sha1Bytes = sha1String.getBytes("UTF-8");
            for(byte b : sha1Bytes){
                sResult += b;
            }
            String passcode = sResult.substring(0, passcodeLength);
            LogcatLogger.getInstance().log(passcode);
            return passcode;
        } catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }


    public static void TestPasscode(){
        PasscodeWebservice("317312" + "12345678" + "20160112103100" + "1234", 8);
    }
}



//    /**
//     * Genera il passcode data la concatenazione dei valori chiave
//     * @param value concatenazione valori
//     * @return il passcode
//     */
//    private static String GeneratePasscode(String value)
//    {
//        // calcola l'hash della Stringa generata, usando l'SHA1
//        String hash = HashUtils.SHA1(value);
//
//        // dal valore hash ricava il passcode
//        return SHA1ToPasscode(hash);
//    }
//
//    /**
//     * Ricava il passcode da una Stringa SHA1
//     * @param sha1 la Stringa SHA1
//     * @return il passcode
//     */
//    private static String SHA1ToPasscode(String sha1)
//    {
//        try {
//            // ottiene i byte della Stringa esadecimale SHA1
//            byte[] sha1Bytes = sha1.getBytes("UTF-8");
//
//            // concatena i byte in un'unica Stringa
//            String concat = "";
//            for (byte b : sha1Bytes) {
//                concat += b;
//            }
//
//            // restituisce una Stringa con i primi n numeri
//            return concat.substring(0, PasscodeLength);
//        } catch (Exception e){
//            e.printStackTrace();
//            return null;
//        }
//    }