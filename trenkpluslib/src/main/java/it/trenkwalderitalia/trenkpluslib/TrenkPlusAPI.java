package it.trenkwalderitalia.trenkpluslib;

import android.os.Build;

import java.io.File;

import it.trenkwalderitalia.models.TWRispostaBaseModel;
import it.trenkwalderitalia.models.request.TWRichiestaAgendaEventiModel;
import it.trenkwalderitalia.models.request.TWRichiestaAggiornaTokenModel;
import it.trenkwalderitalia.models.request.TWRichiestaAnnunciModel;
import it.trenkwalderitalia.models.request.TWRichiestaAnnuncioModel;
import it.trenkwalderitalia.models.request.TWRichiestaArchivioDocumentiModel;
import it.trenkwalderitalia.models.request.TWRichiestaCandidaturaAnnuncioModel;
import it.trenkwalderitalia.models.request.TWRichiestaColloquioRispostaModel;
import it.trenkwalderitalia.models.request.TWRichiestaDocumentoModel;
import it.trenkwalderitalia.models.request.TWRichiestaDocumentoPaginaModel;
import it.trenkwalderitalia.models.request.TWRichiestaErrorReportModel;
import it.trenkwalderitalia.models.request.TWRichiestaFirmaDocumentoModel;
import it.trenkwalderitalia.models.request.TWRichiestaMeetingSettingModel;
import it.trenkwalderitalia.models.request.TWRichiestaNotificheModel;
import it.trenkwalderitalia.models.request.TWRichiestaOTPAttivazioneModel;
import it.trenkwalderitalia.models.request.TWRichiestaOTPCheckCodiceAttivazioneModel;
import it.trenkwalderitalia.models.request.TWRichiestaOTPInfoModel;
import it.trenkwalderitalia.models.request.TWRichiestaQRCodeActionModel;
import it.trenkwalderitalia.models.request.TWRichiestaRapportiLavoroModel;
import it.trenkwalderitalia.models.request.TWRichiestaRdc;
import it.trenkwalderitalia.models.request.TWRichiestaRegistraNuovoUtenteModel;
import it.trenkwalderitalia.models.request.TWRichiestaRichiesteDisponibilitaSaveModel;
import it.trenkwalderitalia.models.request.TWRichiestaTimbraCartellinoModel;
import it.trenkwalderitalia.models.request.TWRichiestaUploadMediaModel;
import it.trenkwalderitalia.models.request.TWRispostaDocumentoModel;
import it.trenkwalderitalia.models.response.TWRispostaAgendaEventiModel;
import it.trenkwalderitalia.models.response.TWRispostaAnnunciModel;
import it.trenkwalderitalia.models.response.TWRispostaAnnuncioCandidaturaModel;
import it.trenkwalderitalia.models.response.TWRispostaAnnuncioModel;
import it.trenkwalderitalia.models.response.TWRispostaArchivioDocumentiCategorieModel;
import it.trenkwalderitalia.models.response.TWRispostaArchivioDocumentiModel;
import it.trenkwalderitalia.models.response.TWRispostaCandidatureDipendenteModel;
import it.trenkwalderitalia.models.response.TWRispostaContatoriModel;
import it.trenkwalderitalia.models.response.TWRispostaDocumentiModel;
import it.trenkwalderitalia.models.response.TWRispostaDownloadModel;
import it.trenkwalderitalia.models.response.TWRispostaFirmaDocumentoModel;
import it.trenkwalderitalia.models.response.TWRispostaLoginModel;
import it.trenkwalderitalia.models.response.TWRispostaMeetingListModel;
import it.trenkwalderitalia.models.response.TWRispostaMeetingSettingListaModel;
import it.trenkwalderitalia.models.response.TWRispostaNewsModel;
import it.trenkwalderitalia.models.response.TWRispostaNotificheModel;
import it.trenkwalderitalia.models.response.TWRispostaOTPAttivazioneModel;
import it.trenkwalderitalia.models.response.TWRispostaOTPInfoModel;
import it.trenkwalderitalia.models.response.TWRispostaRapportiLavoroModel;
import it.trenkwalderitalia.models.response.TWRispostaRegistraNuovoUtenteModel;
import it.trenkwalderitalia.models.response.TWRispostaRichiesteDisponibilitaModel;
import it.trenkwalderitalia.models.response.TWRispostaSalvaRichiestaDisponibilitaModel;
import it.trenkwalderitalia.models.response.TWRispostaSettoriAziendaModel;
import it.trenkwalderitalia.models.response.TWRispostaTipologieContrattualiItemModel;
import it.trenkwalderitalia.models.response.TWRispostaWorkflowStarterFirmaModel;

/**
 * Contiene l'elenco di tutte le chiamate a TrenkPlus che rispettano il protocollo SmartApp
 */
public class TrenkPlusAPI {
    //region / (root)

    //region /SmartApp (area)

    //region SmartApp (controller)
    public static TrenkPlusConnection registraNuovoUtente(TWRichiestaRegistraNuovoUtenteModel model){
        return new TrenkPlusConnectionBuilder<TWRispostaRegistraNuovoUtenteModel>()
                .setResponseClass(TWRispostaRegistraNuovoUtenteModel.class)
                .setMethod(TrenkPlusConnection.Method.POST)
                .setRelativeUrl("SmartApp/SmartApp/RegistraNuovoUtente")
                .setModel(model)
                .build();
    }

    public static TrenkPlusConnection getNotifiche(TWRichiestaNotificheModel model){
        return new TrenkPlusConnectionBuilder<TWRispostaNotificheModel>()
                .setResponseClass(TWRispostaNotificheModel.class)
                .setMethod(TrenkPlusConnection.Method.GET)
                .setRelativeUrl("SmartApp/SmartApp/NotificheUtente")
                .setModel(model)
                .build();
    }

    public static TrenkPlusConnection getAnnunci(TWRichiestaAnnunciModel model){
        return new TrenkPlusConnectionBuilder<TWRispostaAnnunciModel>()
                .setResponseClass(TWRispostaAnnunciModel.class)
                .setMethod(TrenkPlusConnection.Method.GET)
                .setRelativeUrl("/SmartApp/SmartApp/GetAnnunci")
                .setModel(model)
                .build();
    }

    public static TrenkPlusConnection getAnnuncio(TWRichiestaAnnuncioModel model){
        return new TrenkPlusConnectionBuilder<TWRispostaAnnuncioModel>()
                .setResponseClass(TWRispostaAnnuncioModel.class)
                .setMethod(TrenkPlusConnection.Method.GET)
                .setRelativeUrl("/SmartApp/SmartApp/GetAnnuncio")
                .setModel(model)
                .build();
    }

    public static TrenkPlusConnection candidaturaAnnuncio(TWRichiestaCandidaturaAnnuncioModel model){
        return new TrenkPlusConnectionBuilder<TWRispostaAnnuncioCandidaturaModel>()
                .setResponseClass(TWRispostaAnnuncioCandidaturaModel.class)
                .setMethod(TrenkPlusConnection.Method.POST)
                .setRelativeUrl("SmartApp/SmartApp/CandidaturaAnnuncio")
                .setModel(model)
                .build();
    }

    public static TrenkPlusConnection getCandidatureDipendente(){
        return new TrenkPlusConnectionBuilder<TWRispostaCandidatureDipendenteModel>()
                .setResponseClass(TWRispostaCandidatureDipendenteModel.class)
                .setMethod(TrenkPlusConnection.Method.GET)
                .setRelativeUrl("/SmartApp/SmartApp/GetCandidatureDipendente")
                .setModel(null)
                .build();
    }

    public static TrenkPlusConnection getFiliali(){
        return new TrenkPlusConnectionBuilder<>()
                .setResponseClass(TWRispostaBaseModel.class)
                .setMethod(TrenkPlusConnection.Method.GET)
                .setRelativeUrl("SmartApp/SmartApp/GetFiliali")
                .build();
    }

    public static TrenkPlusConnection getDocumentiDaFirmare(){
        return new TrenkPlusConnectionBuilder<TWRispostaDocumentiModel>()
                .setResponseClass(TWRispostaDocumentiModel.class)
                .setMethod(TrenkPlusConnection.Method.GET)
                .setRelativeUrl("SmartApp/SmartApp/GetDocumentiDaFirmare")
                .build();
    }

    public static TrenkPlusConnection notificaIsRead(int idNotifica){
     return new TrenkPlusConnectionBuilder<TWRispostaBaseModel>()
             .setResponseClass(TWRispostaBaseModel.class)
             .setMethod(TrenkPlusConnection.Method.POST)
             .addParameter("IdMessaggio",String.valueOf(idNotifica))
             .setRelativeUrl("Smartapp/smartapp/ConfermaNotificaUtente")
             .build();
    }

//    public static TrenkPlusConnection saveNotificaRisposta(TWRichiestaNotificaRispostaModel model){
//        return new TrenkPlusConnectionBuilder<>()
//                .setResponseClass(TWRispostaBaseModel.class)
//                .setMethod(TrenkPlusConnection.Method.POST)
//                .setRelativeUrl("SmartApp/SmartApp/SaveNotificaRisposta")
//                .setModel(model)
//                .build();
//    }


    public static TrenkPlusConnection aggiornaToken(TWRichiestaAggiornaTokenModel model){
        return new TrenkPlusConnectionBuilder<>()
                .setResponseClass(TWRispostaBaseModel.class)
                .setMethod(TrenkPlusConnection.Method.POST)
                .setRelativeUrl("SmartApp/SmartApp/AggiornaToken")
                .setModel(model)
                .build();
    }


    public static TrenkPlusConnection getRichiesteDisponibilita(TWRichiestaRdc model){
        return new TrenkPlusConnectionBuilder<TWRispostaRichiesteDisponibilitaModel>()
                .setResponseClass(TWRispostaRichiesteDisponibilitaModel.class)
                .setMethod(TrenkPlusConnection.Method.GET)
                .addParameter("IdAziendaCollaboratoriRichiestaDisponibilita", String.valueOf(model.IdAziendaCollaboratoriRichiestaDisponibilita))
                .setRelativeUrl("SmartApp/SmartApp/GetRichiesteDisponibilita")
                .build();
    }

    public static TrenkPlusConnection saveRichiesteDisponibilitaRisposta(TWRichiestaRichiesteDisponibilitaSaveModel model){
        return new TrenkPlusConnectionBuilder<TWRispostaSalvaRichiestaDisponibilitaModel>()
                .setResponseClass(TWRispostaSalvaRichiestaDisponibilitaModel.class)
                .setMethod(TrenkPlusConnection.Method.POST)
                .setRelativeUrl("SmartApp/SmartApp/SaveRichiesteDisponibilitaRisposta")
                .setModel(model)
                .build();
    }


    /**
     * @param model parametri
     * Ottiene informazioni sullo stato del'otp per il device o utente
     */
    public static TrenkPlusConnection dipendenteOTPGetInfo(TWRichiestaOTPInfoModel model){
        return new TrenkPlusConnectionBuilder<TWRispostaOTPInfoModel>()
                .setResponseClass(TWRispostaOTPInfoModel.class)
                .setMethod(TrenkPlusConnection.Method.GET)
                .setRelativeUrl("SmartApp/SmartApp/DipendenteOTPGetInfo")
                .setModel(model)
                .build();
    }


    /**
     * 1)
     * Richiede un codice di attivazione per attivare il servizio
     */
    public static TrenkPlusConnection dipendenteOTPRichiediServizio(){
        return new TrenkPlusConnectionBuilder<>()
                .setResponseClass(TWRispostaBaseModel.class)
                .setMethod(TrenkPlusConnection.Method.GET)
                .setRelativeUrl("SmartApp/SmartApp/DipendenteOTPRichiediServizio")
                .build();
    }

    /**
     * 2)
     * Invia il codice attivazione
     */
    public static TrenkPlusConnection dipendenteOTPCheckCodiceAttivazione(TWRichiestaOTPCheckCodiceAttivazioneModel model){
        return new TrenkPlusConnectionBuilder<>()
                .setResponseClass(TWRispostaBaseModel.class)
                .setMethod(TrenkPlusConnection.Method.POST)
                .setRelativeUrl("SmartApp/SmartApp/DipendenteOTPCheckCodiceAttivazione")
                .setModel(model)
                .build();
    }

    /**
     * 3)
     * attiva il servizio inviando il pin scelto
     */
    public static TrenkPlusConnection dipendenteOTPAttivaServizio(TWRichiestaOTPAttivazioneModel model){
        return new TrenkPlusConnectionBuilder<TWRispostaOTPAttivazioneModel>()
                .setResponseClass(TWRispostaOTPAttivazioneModel.class)
                .setMethod(TrenkPlusConnection.Method.POST)
                .setRelativeUrl("SmartApp/SmartApp/DipendenteOTPAttivaServizio")
                .setModel(model)
                .build();
    }

//    public static TrenkPlusConnection getMeetingList() {
//        return new TrenkPlusConnectionBuilder<TWRispostaMeetingListModel>()
//                .setResponseClass(TWRispostaMeetingListModel.class)
//                .setMethod(TrenkPlusConnection.Method.GET)
//                .setRelativeUrl("SmartApp/SmartApp/GetMeetingList")
//                .build();
//    }

    public static TrenkPlusConnection recuperaPassword(){
        return new TrenkPlusConnectionBuilder<>()
                .setResponseClass(TWRispostaBaseModel.class)
                .setMethod(TrenkPlusConnection.Method.POST)
                .setRelativeUrl("SmartApp/SmartApp/RecuperaPassword")
                .build();
    }

    public static TrenkPlusConnection getNews(){
        return new TrenkPlusConnectionBuilder<TWRispostaNewsModel>()
                .setResponseClass(TWRispostaNewsModel.class)
                .setMethod(TrenkPlusConnection.Method.GET)
                .setRelativeUrl("SmartApp/SmartApp/News")
                .build();
    }

    public static TrenkPlusConnection dipendenteTimbraCartellino(TWRichiestaTimbraCartellinoModel model){
        return new TrenkPlusConnectionBuilder<>()
                .setResponseClass(TWRispostaBaseModel.class)
                .setMethod(TrenkPlusConnection.Method.POST)
                .setRelativeUrl("SmartApp/SmartApp/DipendenteTimbraCartellino")
                .setModel(model)
                .build();
    }

    public static TrenkPlusConnection getAnnunciSettori(){
        return new TrenkPlusConnectionBuilder<TWRispostaSettoriAziendaModel>()
                .setResponseClass(TWRispostaSettoriAziendaModel.class)
                .setMethod(TrenkPlusConnection.Method.GET)
                .setRelativeUrl("SmartApp/SmartApp/GetAnnunciSettori")
                .build();
    }

    public static TrenkPlusConnection saveMediaUtente(TWRichiestaUploadMediaModel model){
        return new TrenkPlusConnectionBuilder<>()
                .setResponseClass(TWRispostaBaseModel.class)
                .setAuthenticated(true)
                .setMethod(TrenkPlusConnection.Method.POST)
                .setRelativeUrl("SmartApp/SmartApp/SaveMediaUtente")
                .setModel(model)
                .setFile(model.file)
                .setFileNameRemote("mediaFile")
                .setFileMimeType(model.mimeType)
                .build();
    }

    public static TrenkPlusConnection getCounters(){
        return new TrenkPlusConnectionBuilder<TWRispostaContatoriModel>()
                .setResponseClass(TWRispostaContatoriModel.class)
                .setMethod(TrenkPlusConnection.Method.GET)
                .setRelativeUrl("SmartApp/SmartApp/GetCounters")
                .build();
    }

    public static TrenkPlusConnection getRapportiLavoro(TWRichiestaRapportiLavoroModel model){
        return new TrenkPlusConnectionBuilder<TWRispostaRapportiLavoroModel>()
                .setResponseClass(TWRispostaRapportiLavoroModel.class)
                .setMethod(TrenkPlusConnection.Method.GET)
                .setRelativeUrl("SmartApp/SmartApp/GetRapportiLavoro")
                .setModel(model)
                .build();
    }

    public static TrenkPlusConnection rispostaRichiestaColloquio(TWRichiestaColloquioRispostaModel model){
        return new TrenkPlusConnectionBuilder<TWRispostaBaseModel>()
                .setResponseClass(TWRispostaBaseModel.class)
                .setMethod(TrenkPlusConnection.Method.POST)
                .setRelativeUrl("SmartApp/SmartApp/RichiestaColloquioRisposta")
                .setModel(model)
                .build();
    }

    public static TrenkPlusConnection getTipologieContrattuali(){
        return new TrenkPlusConnectionBuilder<TWRispostaTipologieContrattualiItemModel>()
                .setResponseClass(TWRispostaTipologieContrattualiItemModel.class)
                .setMethod(TrenkPlusConnection.Method.GET)
                .setRelativeUrl("SmartApp/SmartApp/GetTipologieContrattuali")
                .build();
    }

    //endregion

    //endregion

    //region Archivio (controller)
    public static TrenkPlusConnection getDocumentSummaryByFIDD(TWRichiestaDocumentoModel model){
        return new TrenkPlusConnectionBuilder<TWRispostaDocumentoModel>()
                .setResponseClass(TWRispostaDocumentoModel.class)
                .setMethod(TrenkPlusConnection.Method.GET)
                .setRelativeUrl("Archivio/GetDocumentSummaryByFIDD")
                .setModel(model)
                .build();
    }


    public static TrenkPlusConnection getDocumentImagePageByFIDD(TWRichiestaDocumentoPaginaModel model){
        return new TrenkPlusConnectionBuilder<TWRispostaDownloadModel>()
                .setResponseClass(TWRispostaDownloadModel.class)
                .setMethod(TrenkPlusConnection.Method.GET)
                .setRelativeUrl("Archivio/GetDocumentImagePageByFIDD")
                .setModel(model)
                .build();
    }

    public static TrenkPlusConnection getArchivioDocumentiCategorie(){
        return new TrenkPlusConnectionBuilder<TWRispostaArchivioDocumentiCategorieModel>()
                .setResponseClass(TWRispostaArchivioDocumentiCategorieModel.class)
                .setMethod(TrenkPlusConnection.Method.GET)
                .setRelativeUrl("Archivio/GetArchivioDocumentiCategorie")
                .build();
    }

    public static TrenkPlusConnection getArchivioDocumenti(TWRichiestaArchivioDocumentiModel model){
        return new TrenkPlusConnectionBuilder<TWRispostaArchivioDocumentiModel>()
                .setResponseClass(TWRispostaArchivioDocumentiModel.class)
                .setMethod(TrenkPlusConnection.Method.GET)
                .setRelativeUrl("Archivio/GetArchivioDocumenti")
                .setModel(model)
                .build();
    }


    public static TrenkPlusConnection downloadPDF(String fiddEnc){
        return new TrenkPlusConnectionBuilder<TWRispostaDownloadModel>()
                .setResponseClass(TWRispostaDownloadModel.class)
                .setMethod(TrenkPlusConnection.Method.GET)
                .setRelativeUrl("Archivio/DownloadPDF")
                .addParameter("fiddEnc", fiddEnc)
                .build();
    }
    //endregion

    //region Account (controller)
    public static TrenkPlusConnection login(String username, String password, String deviceId, String idAgenteSmartApp){
        return new TrenkPlusConnectionBuilder<TWRispostaLoginModel>()
                .setResponseClass(TWRispostaLoginModel.class)
                .setAuthenticated(false)
                .setMethod(TrenkPlusConnection.Method.POST)
                .setRelativeUrl("Account/LogOn")
                .addParameter("Username", username)
                .addParameter("Password", password)
                .addParameter("DeviceId", deviceId)
                .addParameter("IdPiattaforma", "4")
                .addParameter("IdAgenteSmartApp", idAgenteSmartApp)
                .addParameter("NomeDispositivo", Build.MODEL)
                .addParameter("DescrizioneDispositivo", Build.DISPLAY)
                .addParameter("AppID", "26")
                .build();
    }

    public static TrenkPlusConnection logout(){
        return new TrenkPlusConnectionBuilder<>()
                .setResponseClass(TWRispostaBaseModel.class)
                .setMethod(TrenkPlusConnection.Method.POST)
                .setRelativeUrl("Account/LogOff")
                .build();
    }
    //endregion

    //region Viewer (controller)
    public static TrenkPlusConnection tryGetDocumentByToken(String idTokenStarterEnc){
        return new TrenkPlusConnectionBuilder<TWRispostaFirmaDocumentoModel>()
                .setResponseClass(TWRispostaFirmaDocumentoModel.class)
                .setMethod(TrenkPlusConnection.Method.GET)
                .setRelativeUrl("Viewer/TryGetDocumentByToken")
                .addParameter("IdTokenStarterEnc", idTokenStarterEnc)
                .build();
    }


    public static TrenkPlusConnection tryGetDocumentByTokenFirma(String idTokenFirmaStarterEnc){
        return new TrenkPlusConnectionBuilder<TWRispostaWorkflowStarterFirmaModel>()
                .setResponseClass(TWRispostaWorkflowStarterFirmaModel.class)
                .setMethod(TrenkPlusConnection.Method.GET)
                .setRelativeUrl("Viewer/TryGetDocumentByTokenFirma")
                .addParameter("IdTokenFirmaStarterEnc", idTokenFirmaStarterEnc)
                .build();
    }

    public static TrenkPlusConnection firmaDocumento(TWRichiestaFirmaDocumentoModel model){
        return new TrenkPlusConnectionBuilder<TWRispostaFirmaDocumentoModel>()
                .setResponseClass(TWRispostaFirmaDocumentoModel.class)
                .setMethod(TrenkPlusConnection.Method.POST)
                .setRelativeUrl("Viewer/FirmaDocumentoV2")
                .setModel(model)
                .build();
    }


    public static TrenkPlusConnection getAgendaEventi(TWRichiestaAgendaEventiModel model){
        return new TrenkPlusConnectionBuilder<TWRispostaAgendaEventiModel>()
                .setResponseClass(TWRispostaAgendaEventiModel.class)
                .setMethod(TrenkPlusConnection.Method.GET)
                .setRelativeUrl("SmartApp/SmartApp/GetAgendaEventi")
                .setModel(model)
                .build();
    }

    public static TrenkPlusConnection qrCodeAction(TWRichiestaQRCodeActionModel model){
        return new TrenkPlusConnectionBuilder<>()
                .setResponseClass(TWRispostaBaseModel.class)
                .setMethod(TrenkPlusConnection.Method.POST)
                .setRelativeUrl("SmartApp/SmartApp/QRCodeAction")
                .setModel(model)
                .build();
    }
    //endregion

    //region Meeting
    public static TrenkPlusConnection getListMeetingSetting(TWRichiestaMeetingSettingModel model){
        return new TrenkPlusConnectionBuilder<TWRispostaMeetingSettingListaModel>()
                .setResponseClass(TWRispostaMeetingSettingListaModel.class)
                .setMethod(TrenkPlusConnection.Method.POST)
                .setRelativeUrl("SmartApp/Meeting/GetListMeetingSetting")
                .setModel(model)
                .build();
    }
    //endregion


    //region Error (controller)
    public static TrenkPlusConnection exceptionLog(TWRichiestaErrorReportModel model){
        return new TrenkPlusConnectionBuilder<>()
                .setResponseClass(TWRispostaBaseModel.class)
                .setAuthenticated(false)
                .setMethod(TrenkPlusConnection.Method.POST)
                .setRelativeUrl("Error/ExceptionLog")
                .setModel(model)
                .build();
    }
    //endregion

    //region Test (controller)


    public static TrenkPlusConnection multipart(int id, byte[] fileBytes, String mimeType, String remoteName){
        return new TrenkPlusConnectionBuilder<>()
                .setResponseClass(TWRispostaBaseModel.class)
                .setAuthenticated(false)
                .setMethod(TrenkPlusConnection.Method.POST)
                .setRelativeUrl("Test/UploadMultipart")
                .addParameter("Int", String.valueOf(id))
                .addParameter("String", "Ciaooo")
                .setFile(fileBytes)
                .setFileNameRemote(remoteName)
                .setFileMimeType(mimeType)
                .build();
    }

    public static TrenkPlusConnection multipart(int id, File file, String mimeType, String remoteName){
        return new TrenkPlusConnectionBuilder<>()
                .setResponseClass(TWRispostaBaseModel.class)
                .setAuthenticated(false)
                .setMethod(TrenkPlusConnection.Method.POST)
                .setRelativeUrl("Test/UploadMultipart")
                .addParameter("Int", String.valueOf(id))
                .addParameter("String", "Ciaooo")
                .setFile(file)
                .setFileNameRemote(remoteName)
                .setFileMimeType(mimeType)
                .build();
    }

    public static TrenkPlusConnection upload(int id, File file, String mimeType, String remoteName){
        return new TrenkPlusConnectionBuilder<>()
                .setResponseClass(TWRispostaBaseModel.class)
                .setAuthenticated(false)
                .setMethod(TrenkPlusConnection.Method.POST)
                .setRelativeUrl("Test/Upload")
                .addParameter("Int", String.valueOf(id))
                .addParameter("String", "Ciaooo")
                .setFile(file)
                .setFileNameRemote(remoteName)
                .setFileMimeType(mimeType)
                .build();
    }


    //endregion
    //endregion

    public static String getFotoCV(String idAgenteEnc){
        return TrenkPlusClient.getAbsoluteUri("Media/GetFotoCV?idAgenteEnc=" + idAgenteEnc);
        //return TrenkPlusClient.getAbsoluteUri("Media/GetFotoCV?IdAgenteTarget=" + idAgenteEnc);
    }

}
