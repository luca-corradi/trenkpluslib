package it.trenkwalderitalia.trenkpluslib.log;

import android.util.Log;

/**
 * Logger di messaggi nel Logcat
 * Per utilizzare la classe utilizzare l'istanza {@link #instance}
 * <pre>
 * Esempio
 * <code>
 *     LogcatLogger.instance.log(tag, error);
 * </code>
 * </pre>
 */
public class LogcatLogger {

    /**
     * Priorità dei log nel logcat.
     *
     * @see android.util.Log
     */
    public static final int LOG_PRORITY = Log.INFO;

    /** Tag dei log */
    public static final String LOG_TAG = "TrkLogcatLogger";

    //region Singleton
    /** L'istanza del logger da utilizzare */
    public static final LogcatLogger instance = new LogcatLogger();

    /** Ottiene l'istanza del logger da utilizzare */
    public static LogcatLogger getInstance(){
        return instance;
    }

    /** Crea una nuova istanza del logger */
    private LogcatLogger(){}
    //endregion

    //region Metodi pubblici
    /**
     * Effettua un log nel logcat
     * @param message il messaggio
     */
    public void log(String message){
        Log.println(LOG_PRORITY, LOG_TAG, message);
    }

    /**
     * Effettua un log nel logcat
     * @param tag etichetta per il messaggio
     * @param message il messaggio
     */
    public void log(String tag, String message){
        Log.println(LOG_PRORITY, tag, message);
    }
    //endregion
}
