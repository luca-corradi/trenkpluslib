package it.trenkwalderitalia.trenkpluslib.log;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;

import it.trenkwalderitalia.trenkpluslib.TrenkPlusClient;
import it.trenkwalderitalia.trenkpluslib.TrenkPlusConnection;
import it.trenkwalderitalia.trenkpluslib.TrenkPlusConnectionBuilder;
import it.trenkwalderitalia.models.TWRispostaBaseModel;
import it.trenkwalderitalia.models.request.TWRichiestaErrorReportModel;

/**
 * Logger di eccezioni che si vogliono tracciare su DB.
 * Invia delle eccezioni a TrenkPlus includendo stacktrace e informazioni di sistema.
 * Per utilizzare la classe utilizzare l'istanza {@link #instance}
 * <pre>
 * Esempio
 * <code>
 *     TrenkPlusLogger.instance.log(context, error);
 * </code>
 * </pre>
 */
public class TrenkPlusLogger {

    //region Singleton
    /** L'istanza del logger da utilizzare */
    public static final TrenkPlusLogger instance = new TrenkPlusLogger();

    /** Ottiene l'istanza del logger da utilizzare */
    public static TrenkPlusLogger getInstance(){
        return instance;
    }

    /** Istanzia un nuovo logger */
    private TrenkPlusLogger(){}
    //endregion


    //region Campi statici
    /**
     * Contiene le informazioni extra da inviare di default ad ogni log delle eccezioni.
     * impostato da {@link TrenkPlusClient#registerForApplication(String, int, String, TrenkPlusClient.LogLevel)}
     */
    public static Map<String,Object> DefaultLogExtraInfo = null;
    //endregion



    // region Metodi pubblici
    /**
     * Effettua il log di una eccezione su Trenk+
     * @param context il contesto nel quale si traccia l'errore
     * @param throwable Errore o eccezione da tracciare
     */
    public void log(Context context, Throwable throwable)
    {
        if(throwable != null)
            log(context, new TWRichiestaErrorReportModel(throwable));
    }

    /**
     * Effettua il log di una eccezione su Trenk+
     * @param context il contesto nel quale si traccia l'errore
     * @param model informazioni sull'errore
     */
    public void log(Context context, TWRichiestaErrorReportModel model)
    {
        if(model != null)
            log(context, null, model.message,model.stacktrace, model.extraInformation);
    }

    /**
     * Effettua il log di una eccezione su Trenk+
     * @param context il contesto nel quale si traccia l'errore
     * @param message il messaggio di errore
     * @param stacktrace stacktrace del codice nel quale si è verificato l'errore
     * @param extraInfo informazioni agiguntive da comunicare
     */
    public void log(Context context, String message, String stacktrace, Map<String, Object> extraInfo) {
        log(context, null, message, stacktrace, extraInfo);
    }

    /**
     * Effettua il log di una eccezione su Trenk+
     * @param context il contesto nel quale si traccia l'errore
     * @param handler gestore della risposta di Trenk+
     * @param message il messaggio di errore
     * @param stacktrace stacktrace del codice nel quale si è verificato l'errore
     * @param extraInfo informazioni agiguntive da comunicare
     */
    public void log(Context context, TrenkPlusClient.Handler handler, String message, String stacktrace, Map<String, Object> extraInfo) {
        Map<String,Object> mergedExtraInfo = mergeExtraAttributes(extraInfo);
        String extraInfoJson = new GsonBuilder()
                .setPrettyPrinting()
                .disableHtmlEscaping()
                .create()
                .toJson(mergedExtraInfo);
        log(context, handler, message, stacktrace, extraInfoJson);
    }


    //endregion

    //region Metodi privati
    /**
     * Effettua il log di una eccezione su Trenk+
     * @param context il contesto nel quale si traccia l'errore
     * @param handler gestore della risposta di Trenk+
     * @param message il messaggio di errore
     * @param stacktrace stacktrace del codice nel quale si è verificato l'errore
     * @param extraInfo informazioni agiguntive da comunicare, sottoforma di stringa JSON
     */
    private void log(Context context, TrenkPlusClient.Handler handler, String message, String stacktrace, String extraInfo) {
        TrenkPlusConnection trkpConn = new TrenkPlusConnectionBuilder<>()
                .setMethod(TrenkPlusConnection.Method.POST)
                .setController("Error")
                .setAction("ExceptionLog")
                .addParameter("Message", message)
                .addParameter("StackTrace", stacktrace)
                .addParameter("ExtraInformation", extraInfo)
                .build();

        new TrenkPlusClient(context, handler).execute(trkpConn);
    }

    /**
     * Unisce le informazioni aggiuntive di default con quelle specificate
     * @param extraInfo informazioni da aggiungere
     * @return l'unione delle informazioni
     */
    private Map<String, Object> mergeExtraAttributes(Map<String,Object> extraInfo) {
        if(extraInfo == null)
            return DefaultLogExtraInfo;

        if(DefaultLogExtraInfo != null){
            HashMap<String, Object> merged = new HashMap<>(DefaultLogExtraInfo);
            for (String key : extraInfo.keySet()){
                // le extra info passate sovrascrivono le eventuali di default
                merged.put(key, extraInfo.get(key));
            }
            return merged;
        }
        return extraInfo;
    }
    //endregion
}
