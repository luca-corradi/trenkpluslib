package it.trenkwalderitalia.trenkpluslib;

import com.google.common.collect.Iterators;
import com.google.common.collect.Maps;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.net.URI;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import it.trenkwalderitalia.trenkpluslib.utils.ReflectionUtils;
import it.trenkwalderitalia.models.TWRispostaBaseModel;

/**
 * Builder per costruire i dettagli di una chiamata a Trenk Plus.
 */
@SuppressWarnings("unused")
public class TrenkPlusConnectionBuilder<T extends TWRispostaBaseModel> {
    private TrenkPlusConnection<T> connection;

    //region Costruttori
    public TrenkPlusConnectionBuilder(){
        connection = new TrenkPlusConnection<>();
    }

    public TrenkPlusConnectionBuilder(TrenkPlusConnection.Method method, String relativeUrl){
        connection = new TrenkPlusConnection<>(method, relativeUrl);
    }
    //endregion

    //region Metodi pubblici
    public TrenkPlusConnectionBuilder setMethod(TrenkPlusConnection.Method method){
        connection.method = method;
        return this;
    }

    public TrenkPlusConnectionBuilder setRelativeUrl(String relativeUrl){
        connection.setRelativeUrl(relativeUrl);
        return this;
    }

    public TrenkPlusConnectionBuilder setArea(String area){
        connection.area = area;
        return this;
    }

    public TrenkPlusConnectionBuilder setController(@NotNull String controller){
        connection.controller = controller;
        return this;
    }

    public TrenkPlusConnectionBuilder setAction(@NotNull String action){
        connection.action = action;
        return this;
    }

//    public TrenkPlusConnectionBuilder setParameters(Dictionary<String, String> parameters){
//        Iterator<String> keysIterator = Iterators.forEnumeration(parameters.keys());
//        connection.parameters = Maps.toMap(keysIterator, parameters::get);
//        return this;
//    }

    public TrenkPlusConnectionBuilder setParameters(Map<String, String> parameters){
        connection.parameters = parameters;
        return this;
    }

    public TrenkPlusConnectionBuilder addParameters(Map<String, String> parameters){
        if(!connection.hasParameters())
            connection.parameters = parameters;
        else
            connection.parameters.putAll(parameters);

        return this;
    }

    public TrenkPlusConnectionBuilder addParameter(@NotNull String name, String value){
        if(!connection.hasParameters())
            connection.parameters = new HashMap<String,String>();

        connection.parameters.put(name, value);
        return this;
    }

    public TrenkPlusConnectionBuilder setModel(Object model){
        if(model != null){
            Map<String, String> parameters = ReflectionUtils.toParameters(model);
            setParameters(parameters);
        }
        return this;
    }

    public TrenkPlusConnectionBuilder addModel(Object model){
        if(model != null){
            Map<String, String> parameters = ReflectionUtils.toParameters(model);
            addParameters(parameters);
        }
        return this;
    }

    public TrenkPlusConnectionBuilder setFile(@NotNull byte[] fileBytes){
        connection.fileBytes = fileBytes;
        return this;
    }

    public TrenkPlusConnectionBuilder setFile(@NotNull File file){
        connection.file = file;
        return this;
    }

    public TrenkPlusConnectionBuilder setFile(@NotNull String filePath){
        connection.file = new File(filePath);
        return this;
    }

    public TrenkPlusConnectionBuilder setFile(@NotNull URI fileURI){
        connection.file = new File(fileURI);
        return this;
    }

    public TrenkPlusConnectionBuilder setAuthenticated(boolean value){
        connection.isAuthenticated = value;
        return this;
    }

    public TrenkPlusConnectionBuilder setResponseClass(Class<T> responseClass){
        connection.responseClass = responseClass;
        return this;
    }

    public TrenkPlusConnection<T> build(){
        return connection;
    }

    public TrenkPlusConnectionBuilder setFileNameRemote(String remoteName) {
        connection.fileNameRemote = remoteName;
        return this;
    }

    public TrenkPlusConnectionBuilder setFileMimeType(String mimeType){
        connection.fileMimeType = mimeType;
        return this;
    }
    //endregion
}
