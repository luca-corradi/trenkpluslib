package it.trenkwalderitalia.trenkpluslib;

import android.net.Uri;
import android.text.TextUtils;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import it.trenkwalderitalia.models.TWRispostaBaseModel;

/**
 * Rappresenta una chiamata ad una action su Trenk Plus,
 * secondo le specifiche del protocollo SmartApp, ovvero un client HTTP che:
 * - si presenta con uno User Agent TWSMARTAPP ({extra info}..)
 * - riceve i dati in risposta in JSON dove ognuna ha un Errore e Descrizione ed eventuali dati
 */
public class TrenkPlusConnection<T extends TWRispostaBaseModel> implements Serializable {

    //region Costanti
    public static final int OK = 0;
    //endregion


    //region Costruttori
    @SuppressWarnings("unused")
    public TrenkPlusConnection() {
    }

    @SuppressWarnings("unused")
    public TrenkPlusConnection(Method method, String relativeUrl){
        setRelativeUrl(relativeUrl);
    }

    @SuppressWarnings("unused")
    public TrenkPlusConnection(Method method, @NotNull String controller, @NotNull String action) {
        initialize(method, null, controller, action, null);
    }

    @SuppressWarnings("unused")
    public TrenkPlusConnection(Method method, String area, @NotNull String controller, @NotNull String action) {
        initialize(method, area, controller, action, null);
    }

    @SuppressWarnings("unused")
    public TrenkPlusConnection(Method method, String area, @NotNull String controller, @NotNull String action,
                               Class<T> responseClass){
        initialize(method, area, controller, action, responseClass);
    }

    private void initialize(Method method, String area, @NotNull String controller, @NotNull String action,
                            Class<T> responseClass){
        this.method = method;
        this.area = area;
        this.controller = controller;
        this.action = action;
        this.responseClass = responseClass;
    }
    //endregion


    //region Campi pubblici
    /** L'area in cui si trova la action da chiamare */
    public String area;

    /** Il controller in cui si trova la action da chiamare */
    public String controller;

    /** Il nome della action da chiamare */
    public String action;

    /** Metodo HTTP con il quale richiamare l'action */
    public Method method;

    /** Parametri da inviare nella richiesta */
    public Map<String,String> parameters;

    /** Classe rappresentante i dati restituiti dalla chiamata. Alternativa di {@see responseType} */
    public Class<T> responseClass;

    /** Tipo di dati restituiti dalla chiamata. Alternativa di {@see responseClass} */
    public Type responseType;

    /** Indica se la chiamata necessita l'autenticazione */
    public boolean isAuthenticated = true;

    /** Il file da inviare */
    public File file;

    /** I byte del file da inviare. Alternativa a {@link #file} */
    public byte[] fileBytes;

    /**
     * Il nome del file lato server (corrisponde allo stesso nome del parametro di tipo
     * HttpPostedFile nella action da chiamare)
     */
    public String fileNameRemote;

    /** Il MIME Tyipe del file */
    public String fileMimeType;
    //endregion


    //region Metodi pubblici

    /**
     * @return Indica se la connessione è il login
     */
    public boolean isLogin() {
        return this.toString().equalsIgnoreCase("/Account/LogOn");
    }

    /**
     * @return Indica se la connessione prevede un upload di file
     */
    public boolean hasFile(){
        return (file != null && file.isFile())
                || fileBytes != null;
    }

    /**
     * @return Indica se l'invio deve essere di tipo multipart,
     * poichè ci sono sia parametri che un file da inviare
     */
    public boolean isMultipart(){
        return hasFile() && hasParameters();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder((area != null && !area.isEmpty())
                ? String.format("/%s/%s/%s", area,controller,action)
                : String.format("/%s/%s", controller,action));

        if(hasParameters() && (method == Method.GET || isMultipart())){
            // aggiunge i parametri alla querystring nell'url
            sb.append("?");

            List<String> paramsList = new ArrayList<String>();
            for (String key: parameters.keySet()){
                try {
                    paramsList.add(String.format("%s=%s", key, URLEncoder.encode(parameters.get(key), "UTF-8")));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            sb.append(TextUtils.join("&", paramsList));
        }

        return sb.toString();
    }

    /** Indica se ci sono parametri da inviare */
    public boolean hasParameters(){
        return parameters != null && !parameters.isEmpty();
    }

    /** Ottiene la querystring codificata contenente i parametri da inviare */
    public String getParametersQueryString(){
        if(!hasParameters())
            return "";

        Uri.Builder builder = new Uri.Builder();
        for (Map.Entry<String,String> param : parameters.entrySet())
            builder.appendQueryParameter(param.getKey(), param.getValue());

        return builder.build().getEncodedQuery();
    }

    /**
     * Costruisce una rappresentazione leggibile dei parametri da inviare
     * Da usare per scopo di log.
     * @return la stampa dei parametri
     */
    public String logParameters(){
        if(!hasParameters())
            return "";

        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String,String> param : parameters.entrySet())
            sb.append(String.format("\t%s:\t%s\n", param.getKey(), param.getValue()));

        return sb.toString();
    }

    /**
     * @return Ottiene il metodo come stringa
     */
    public String getMethodString() {
        String me = String.valueOf(method);
        return me;
    }

    /**
     * Interpreta il relative url specificato e ne deduce l'eventuale area, il controller e l'action
     * @param relativeUrl url relativo, partendo dalla root del sito
     */
    public void setRelativeUrl(String relativeUrl){
        if(!TextUtils.isEmpty(relativeUrl)){

            // rimuove l'eventuale carattere '/' in testa
            if(relativeUrl.startsWith("/"))
                relativeUrl = relativeUrl.substring(1);

            String[] tokens = TextUtils.split(relativeUrl,"/");
            if(tokens != null && tokens.length >= 2){
                /* l'url è valido, può essere
                    /controller/action
                    /area/controller/action
                */
                if (tokens.length == 2)
                    initialize(method, null, tokens[0], tokens[1], responseClass);
                else
                    initialize(method, tokens[0], tokens[1], tokens[2], responseClass);
            }
        }
    }
    //endregion


    //region Componenti interne
    public enum Method {
        GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE
    }

    //endregion
}
