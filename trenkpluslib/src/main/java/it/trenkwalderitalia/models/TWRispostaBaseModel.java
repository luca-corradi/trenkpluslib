package it.trenkwalderitalia.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import it.trenkwalderitalia.trenkpluslib.TrenkPlusConnection;

public class TWRispostaBaseModel implements Serializable {

    private static final int DEFAULT_ERROR = -8;
    private static final String DEFAULT_ERROR_MESSAGE = "Errore nella connessione al server";

    @SerializedName("Errore")
    public int errore;
    @SerializedName("Descrizione")
    public String descrizione;

    public static TWRispostaBaseModel newError(String message){
        return new TWRispostaBaseModel(-1,message);
    }

    public static boolean isOk(TWRispostaBaseModel response){
        return response != null && response.isOk();
    }

    public TWRispostaBaseModel (){
        errore = 0;
        descrizione = "";
    }

    public TWRispostaBaseModel (int error, String message){
        this.errore = error;
        this.descrizione = message;
    }

    public TWRispostaBaseModel (TWRispostaBaseModel model){
        if(model != null) {
            this.errore = model.errore;
            this.descrizione = model.descrizione;
        }
        else {
            this.errore = DEFAULT_ERROR;
            this.descrizione = DEFAULT_ERROR_MESSAGE;
        }
    }

    public boolean isOk(){
        return errore == TrenkPlusConnection.OK;
    }
}
