package it.trenkwalderitalia.models.response;

import it.trenkwalderitalia.models.TWRispostaBaseModel;
import it.trenkwalderitalia.trenkpluslib.TrenkPlusClient;

public class TWRispostaFileModel extends TWRispostaBaseModel {

    public TWRispostaFileModel(){}
    public TWRispostaFileModel(String filePath){
        this.filePath = filePath;
    }

    public String filePath;

    public TWRispostaFileModel(TWRispostaBaseModel response) {
        super(response);
    }
}
