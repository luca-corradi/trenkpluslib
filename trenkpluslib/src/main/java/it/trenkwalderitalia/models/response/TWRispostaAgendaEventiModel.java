package it.trenkwalderitalia.models.response;

import java.util.List;

import it.trenkwalderitalia.models.TWRispostaBaseModel;
import it.trenkwalderitalia.models.entity.TWAgendaEventiModel;

/**
 * Created by moncigoale on 03/03/2016.
 */
public class TWRispostaAgendaEventiModel extends TWRispostaBaseModel {
    public List<TWAgendaEventiModel> Dati;

}
