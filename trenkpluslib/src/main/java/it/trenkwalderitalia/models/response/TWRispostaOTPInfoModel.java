package it.trenkwalderitalia.models.response;

import it.trenkwalderitalia.models.TWRispostaBaseModel;

public class TWRispostaOTPInfoModel extends TWRispostaBaseModel {
    public int OTPStato;
    public boolean OTPISAttivo;
    public String OTPCellulare;
    public String PinEnc;
}
