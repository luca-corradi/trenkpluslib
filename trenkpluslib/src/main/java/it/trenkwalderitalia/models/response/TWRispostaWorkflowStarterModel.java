package it.trenkwalderitalia.models.response;

import it.trenkwalderitalia.models.TWRispostaBaseModel;
import it.trenkwalderitalia.models.entity.TWWorkflowStarterModel;


public class TWRispostaWorkflowStarterModel extends TWRispostaBaseModel {

    public TWRispostaWorkflowStarterModel(TWRispostaBaseModel model){
        super(model);
    }

    public TWWorkflowStarterModel Dati;
}
