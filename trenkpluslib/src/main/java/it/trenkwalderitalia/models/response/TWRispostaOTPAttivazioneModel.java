package it.trenkwalderitalia.models.response;

import it.trenkwalderitalia.models.TWRispostaBaseModel;

public class TWRispostaOTPAttivazioneModel extends TWRispostaBaseModel {
    public TWRispostaOTPAttivazioneModel(TWRispostaBaseModel model){
        super(model);
    }

    public int IdAgentePin;
    public String UserToken;
}
