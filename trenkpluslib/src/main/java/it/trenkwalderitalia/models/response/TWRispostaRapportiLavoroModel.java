package it.trenkwalderitalia.models.response;

import java.util.List;

import it.trenkwalderitalia.models.TWRispostaBaseModel;
import it.trenkwalderitalia.models.entity.TWContrattoTipologiaModel;

public class TWRispostaRapportiLavoroModel extends TWRispostaBaseModel {
    public List<TWContrattoTipologiaModel> Dati;
}
