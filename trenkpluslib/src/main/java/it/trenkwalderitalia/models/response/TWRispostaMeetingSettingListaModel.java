package it.trenkwalderitalia.models.response;

import java.util.List;

import it.trenkwalderitalia.models.TWRispostaBaseModel;
import it.trenkwalderitalia.models.entity.TWMeetingSettingModel;

public class TWRispostaMeetingSettingListaModel extends TWRispostaBaseModel {
    public List<TWMeetingSettingModel> Dati;

    public TWRispostaMeetingSettingListaModel(TWRispostaBaseModel response) {
        super(response);
    }
}
