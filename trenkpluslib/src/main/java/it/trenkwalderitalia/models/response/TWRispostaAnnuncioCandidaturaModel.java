package it.trenkwalderitalia.models.response;

import it.trenkwalderitalia.models.TWRispostaBaseModel;

/**
 * Created by moncigoale on 07/03/2016.
 */
public class TWRispostaAnnuncioCandidaturaModel extends TWRispostaBaseModel {
    public int IDSurveyAgente;
    public String IDSurveyAgenteEnc;

    public String getSurveyUrl(){
        //TODO
        return String.format("https://staging.trenkwalderitalia.it/PersonalArea/Home/Survey?IdSurveyAgenteEnc=%s", IDSurveyAgenteEnc);
    }
}
