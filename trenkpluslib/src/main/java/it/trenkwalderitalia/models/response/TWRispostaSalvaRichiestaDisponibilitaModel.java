package it.trenkwalderitalia.models.response;

import it.trenkwalderitalia.models.TWRispostaBaseModel;
import it.trenkwalderitalia.trenkpluslib.TrenkPlusClient;

public class TWRispostaSalvaRichiestaDisponibilitaModel extends TWRispostaBaseModel {
    public int IDSurveyAgente;
    public String IDSurveyAgenteEnc;

    public String getSurveyUrl(){
        //TODO
        return String.format("https://staging.trenkwalderitalia.it/PersonalArea/Home/Survey?IdSurveyAgenteEnc=%s", IDSurveyAgenteEnc);
    }
}
