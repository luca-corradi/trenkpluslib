package it.trenkwalderitalia.models.response;

import java.util.List;

import it.trenkwalderitalia.models.TWRispostaBaseModel;
import it.trenkwalderitalia.models.entity.TWTipologiaContrattualeItemModel;

/**
 * Created by moncigoale on 24/03/2016.
 */
public class TWRispostaTipologieContrattualiItemModel extends TWRispostaBaseModel {
    public List<TWTipologiaContrattualeItemModel> Dati;
}
