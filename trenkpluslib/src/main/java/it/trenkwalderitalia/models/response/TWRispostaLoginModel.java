package it.trenkwalderitalia.models.response;

import java.util.Date;

import it.trenkwalderitalia.models.TWRispostaBaseModel;
import it.trenkwalderitalia.models.entity.CustomLayoutModel;

public class TWRispostaLoginModel extends TWRispostaBaseModel {


    public TWRispostaLoginModel(TWRispostaBaseModel model){
        super(model);
    }

    /**  id dell'agente */
    public int IdAgente;

    /**  id del dipendente */
    public int IdDipendente;

    /**  Memorizza il tipo dell'agente */
    public int TipoAgente;

    /**  id dell'agente in formato stringa */
    public String IdAgenteString;

    /**  Cognome e nome dell'utente */
    public String CognomeNome;

    /**  id dell'azienda */
    public int IdAzienda;

    /**  Nome dell'azienda */
    public String NomeAzienda;

    /**  id filiale di riferimento */
    public int IdFilialeRiferimento;

    /**  Nome della filiale di riferimento */
    public String FilialeDiRiferimento;

    /**  Indica se l'ambiente demo è abilitata per l'utente corrente */
    public boolean IsAbilitatoAmbienteDemo;

    /**  id registrazione */
    public String IDRegistrazione;

    /**  id application log */
    public int IdApplicationLog;

    /**  Indica quanti link utente ha l'agente */
    public int LinksUtenteCount;

    /**  Stato del software licence agreement */
    public int StatoSoftwareLicenceAgreement;

    /**  FIDD del documento del software licence agreement */
    public int FIDDSoftwareLicenceAgreement;

    /**  FIDD criptato del documento del software licence agreement */
    public String FIDDSoftwareLicenceAgreementEnc;

    /**  Indica se l'utente è certificato */
    public boolean IsCertificato;

    /**  Stato di aggiornamento dell'application */
    public int TipoAggiornamentoApplicationVersionCheck;

    /**  Descrizione dell'aggiornamento dell'app */
    public String DescrizioneAggiornamentoApplicationVersionCheck;

    /**  Email dell'agente */
    public String Email;

    /**  Orario del server sottoforma di stringa (x Android) */
    public String ServerTime;

    /**  Orario del server */
    public Date ServerDateTime;

    /**  Indica se l'utente è abilitato alle timbrature da app */
    public boolean TimbriAPP;

    /**  Indica lo stato del servizio dell'OTP relativo all'IdAgenteSmartApp che ha effettuato il login */
    public int OTPStato;

    /**  Indica se il servizio OTP è abilitato per l'IdAgente (su almeno uno dei suoi IdAgenteSmartApp) */
    public boolean OTPIsAttivo;

    /**  Il numero di cellulare abilitato all'OTP */
    public String OTPCellulare;

    /**  Il numero di cellulare abilitato all'OTP, semioscurato */
    public String OTPCellulareEnc;

    /**  Il PIN dell'OTP hashato dell'utente, se ce l'ha */
    public String PinEnc;

    /** Memorizzati le informazioni dell'azienda a cui appartiene l'utente dell'app (brand) */
    public CustomLayoutModel CustomLayout ;
}