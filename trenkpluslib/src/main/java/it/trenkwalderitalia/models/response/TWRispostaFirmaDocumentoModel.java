package it.trenkwalderitalia.models.response;

import it.trenkwalderitalia.models.TWRispostaBaseModel;
import it.trenkwalderitalia.models.entity.TWFirmaDocumentoStarterModel;

public class TWRispostaFirmaDocumentoModel extends TWRispostaBaseModel{
    public TWRispostaFirmaDocumentoModel(TWRispostaBaseModel model){
        super(model);
    }

    public TWFirmaDocumentoStarterModel Dati;
}
