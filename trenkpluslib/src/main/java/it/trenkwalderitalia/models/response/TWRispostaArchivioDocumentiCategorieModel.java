package it.trenkwalderitalia.models.response;

import java.util.List;

import it.trenkwalderitalia.models.TWRispostaBaseModel;
import it.trenkwalderitalia.models.entity.TWArchivioDocumentiCategoriaModel;

public class TWRispostaArchivioDocumentiCategorieModel extends TWRispostaBaseModel {
    public List<TWArchivioDocumentiCategoriaModel> Dati;
}
