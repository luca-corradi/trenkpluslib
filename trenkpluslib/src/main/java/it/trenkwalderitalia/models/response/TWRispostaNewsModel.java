package it.trenkwalderitalia.models.response;

import java.util.List;

import it.trenkwalderitalia.models.TWRispostaBaseModel;
import it.trenkwalderitalia.models.entity.TWNewsModel;

public class TWRispostaNewsModel extends TWRispostaBaseModel {
    public List<TWNewsModel> Dati;

    public TWRispostaNewsModel(TWRispostaBaseModel response) {
        super(response);
    }
}
