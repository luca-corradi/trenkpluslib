package it.trenkwalderitalia.models.response;

import java.util.List;

import it.trenkwalderitalia.models.TWRispostaBaseModel;
import it.trenkwalderitalia.models.entity.TWCandidaturaModel;

/**
 * Created by moncigoale on 13/01/2016.
 */
public class TWRispostaCandidatureDipendenteModel extends TWRispostaBaseModel {

    public TWRispostaCandidatureDipendenteModel(TWRispostaBaseModel model){
        super(model);
    }

    public List<TWCandidaturaModel> Candidature;
    public int Count;

}
