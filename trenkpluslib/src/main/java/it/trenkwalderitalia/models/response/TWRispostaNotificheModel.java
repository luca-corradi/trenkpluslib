package it.trenkwalderitalia.models.response;

import java.util.List;

import it.trenkwalderitalia.models.TWRispostaBaseModel;
import it.trenkwalderitalia.models.entity.TWNotificaModel;
import it.trenkwalderitalia.models.entity.TWNotificaTipoModel;

public class TWRispostaNotificheModel extends TWRispostaBaseModel {

    public TWRispostaNotificheModel(TWRispostaBaseModel model){
        super(model);
    }

    public List<TWNotificaModel> Notifiche;

    /**  Numero di notifiche */
    public int Count;

    /**  Numero di notifiche da leggere */
    public int CountDaLeggere;

    /**  Tipi di notifiche */
    public List<TWNotificaTipoModel> TipiNotifiche;

    /**  Numero di notifiche tipo */
    public int CountTipiNotifiche;
}
