package it.trenkwalderitalia.models.response;

import java.util.List;

import it.trenkwalderitalia.models.TWRispostaBaseModel;
import it.trenkwalderitalia.models.entity.TWSettoreAziendaModel;

public class TWRispostaSettoriAziendaModel extends TWRispostaBaseModel {
    public List<TWSettoreAziendaModel> Dati;

    public TWRispostaSettoriAziendaModel(TWRispostaBaseModel response) {
        super(response);
    }
}
