package it.trenkwalderitalia.models.response;

import java.util.List;

import it.trenkwalderitalia.models.TWRispostaBaseModel;
import it.trenkwalderitalia.models.entity.TWProvinciaModel;

public class TWRispostaProvinceModel extends TWRispostaBaseModel {
    public List<TWProvinciaModel> Dati;
}
