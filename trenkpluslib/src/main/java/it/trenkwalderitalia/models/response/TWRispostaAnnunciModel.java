package it.trenkwalderitalia.models.response;

import java.util.List;

import it.trenkwalderitalia.models.TWRispostaBaseModel;
import it.trenkwalderitalia.models.entity.TWAnnunciModel;

public class TWRispostaAnnunciModel extends TWRispostaBaseModel{

    public TWRispostaAnnunciModel(TWRispostaBaseModel model){
        super(model);
    }

    public List<TWAnnunciModel> Annunci;
    public int Count;
    public int TotalCount;
}
