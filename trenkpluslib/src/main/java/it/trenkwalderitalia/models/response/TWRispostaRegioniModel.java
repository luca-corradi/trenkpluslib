package it.trenkwalderitalia.models.response;

import java.util.List;

import it.trenkwalderitalia.models.TWRispostaBaseModel;
import it.trenkwalderitalia.models.entity.TWRegioneModel;

public class TWRispostaRegioniModel extends TWRispostaBaseModel {
    public List<TWRegioneModel> Dati;
}
