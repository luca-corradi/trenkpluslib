package it.trenkwalderitalia.models.response;


import it.trenkwalderitalia.models.TWRispostaBaseModel;

public class TWRispostaDownloadModel extends TWRispostaBaseModel {

    public TWRispostaDownloadModel(TWRispostaBaseModel model){
        super(model);
    }

    public TWRispostaDownloadModel(){}
    public TWRispostaDownloadModel(byte[] bytes){
        content = bytes;
    }

    public byte[] content;
}
