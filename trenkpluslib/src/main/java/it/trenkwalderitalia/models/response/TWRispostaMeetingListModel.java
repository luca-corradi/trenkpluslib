package it.trenkwalderitalia.models.response;

import java.util.List;

import it.trenkwalderitalia.models.TWRispostaBaseModel;
import it.trenkwalderitalia.models.entity.TWMeetingModel;

public class TWRispostaMeetingListModel extends TWRispostaBaseModel {

    public TWRispostaMeetingListModel(TWRispostaBaseModel model){
        super(model);
    }

    public List<TWMeetingModel> Dati;
}
