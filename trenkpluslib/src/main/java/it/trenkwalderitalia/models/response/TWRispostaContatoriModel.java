package it.trenkwalderitalia.models.response;

import it.trenkwalderitalia.models.TWRispostaBaseModel;
import it.trenkwalderitalia.models.entity.TWContatoriModel;

public class TWRispostaContatoriModel extends TWRispostaBaseModel {
    public TWContatoriModel Dati;

    public TWRispostaContatoriModel(TWRispostaBaseModel response) {
        super(response);
    }
}
