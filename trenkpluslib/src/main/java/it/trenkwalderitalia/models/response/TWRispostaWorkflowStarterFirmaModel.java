package it.trenkwalderitalia.models.response;


import it.trenkwalderitalia.models.TWRispostaBaseModel;
import it.trenkwalderitalia.models.entity.TWWorkflowStarterFirmaModel;

public class TWRispostaWorkflowStarterFirmaModel extends TWRispostaBaseModel{

    public TWRispostaWorkflowStarterFirmaModel(TWRispostaBaseModel model){
        super(model);
    }

    public TWWorkflowStarterFirmaModel Dati;
}
