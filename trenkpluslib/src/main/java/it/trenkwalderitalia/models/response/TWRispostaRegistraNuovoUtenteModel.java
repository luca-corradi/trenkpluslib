package it.trenkwalderitalia.models.response;

import java.util.List;

import it.trenkwalderitalia.models.TWRispostaBaseModel;
import it.trenkwalderitalia.models.entity.TWAccountUtenteModel;
import it.trenkwalderitalia.models.entity.TWValidationResultModel;

public class TWRispostaRegistraNuovoUtenteModel extends TWRispostaBaseModel
{
    public TWRispostaRegistraNuovoUtenteModel(TWRispostaBaseModel model){
        super(model);
    }

    public TWAccountUtenteModel AccountUtente;
    public List<TWValidationResultModel> ValidationResults;
    public int ValidationResultsCount;
}

