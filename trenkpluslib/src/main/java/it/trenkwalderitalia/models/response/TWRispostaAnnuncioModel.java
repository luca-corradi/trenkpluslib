package it.trenkwalderitalia.models.response;

import it.trenkwalderitalia.models.TWRispostaBaseModel;
import it.trenkwalderitalia.models.entity.TWAnnunciModel;

/**
 * Created by moncigoale on 13/01/2016.
 */
public class TWRispostaAnnuncioModel extends TWRispostaBaseModel {

    public TWRispostaAnnuncioModel(TWRispostaBaseModel model){
        super(model);
    }

    /**  Annuncio */
    public TWAnnunciModel Annuncio;

    /**  Restituisce se un annuncio è valido */
    public boolean IsAnnuncioValido;

    /**  Restituisce se l'utente corrente è candidato */
    public boolean IsUtenteCandidato;

    /**  Indica lo stato della candidatura. Se 0 l'utente è già candidato. Se minore di 0 l'utente ha un cv da correggere ma può candidarsi. Se maggiore di 0 l'utente può candidarsi */
    public int StatoCandidabilita;

    /**  Id del record della visita */
    public int IdCmsAnnunciProfiliCandidati;
}
