package it.trenkwalderitalia.models.response;


import java.util.List;

import it.trenkwalderitalia.models.TWRispostaBaseModel;
import it.trenkwalderitalia.models.entity.TWDocumentoDaFirmareModel;
import it.trenkwalderitalia.models.entity.TWDocumentoModel;

public class TWRispostaDocumentiModel extends TWRispostaBaseModel {

    public TWRispostaDocumentiModel(TWRispostaBaseModel model){
        super(model);
    }

    public List<TWDocumentoDaFirmareModel> Dati;
}
