package it.trenkwalderitalia.models.response;


import java.util.List;

import it.trenkwalderitalia.models.TWRispostaBaseModel;
import it.trenkwalderitalia.models.entity.TWRichiestaDisponibilitaModel;

public class TWRispostaRichiesteDisponibilitaModel extends TWRispostaBaseModel {

    public TWRispostaRichiesteDisponibilitaModel(TWRispostaBaseModel model){
        super(model);
    }

    public List<TWRichiestaDisponibilitaModel> RichiesteDisponibilita;
}
