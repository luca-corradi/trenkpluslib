package it.trenkwalderitalia.models.util;

import java.io.Serializable;

public interface ListGroup extends Serializable {
    boolean isHeader();
    String getHeader();
}
