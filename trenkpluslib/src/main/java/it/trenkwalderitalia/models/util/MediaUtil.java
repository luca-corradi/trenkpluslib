package it.trenkwalderitalia.models.util;

import android.webkit.MimeTypeMap;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class MediaUtil {

    public static final int TIPO_FOTO = 1;
    public static final int TIPO_VIDEO = 2;
    public static final int TIPO_AUDIO = 4;
    public static final List<Integer> TYPES = Arrays.asList(TIPO_AUDIO, TIPO_FOTO, TIPO_VIDEO);

    public static String getMimeType(String filePath) {
        if(filePath == null)
            return null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(filePath);
        return MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
    }

    public static String getMimeType(File file) {
        return getMimeType(file != null ? file.getPath() : null);
    }


}
