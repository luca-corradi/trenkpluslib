package it.trenkwalderitalia.models.util;


public class ImageInfo {
    private int imageHeight;
    private int imageWidth;
    private String imageType;

    public ImageInfo(){}
    public ImageInfo(int imageHeight, int imageWidth){

    }
    public ImageInfo(int imageHeight, int imageWidth, String imageType){
        this.imageHeight = imageHeight;
        this.imageHeight = imageWidth;
        this.imageType = imageType;
    }

    public int getImageHeight() {
        return imageHeight;
    }

    public void setImageHeight(int imageHeight) {
        this.imageHeight = imageHeight;
    }

    public int getImageWidth() {
        return imageWidth;
    }

    public void setImageWidth(int imageWidth) {
        this.imageWidth = imageWidth;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }
}
