package it.trenkwalderitalia.models.entity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by corradiluc on 14/01/2016.
 */
public class TWDocumentoFirmeModel  implements Serializable {
    /**  Stato firma del documento */
    public int StatoApprovazione;

    /**  Elenco delle firme del documento */
    public List<TWDocumentoFirmaItemModel> FirmeList;
}
