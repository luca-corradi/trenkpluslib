package it.trenkwalderitalia.models.entity;

import java.io.Serializable;
import java.util.Date;

public class TWContrattoTipologiaModel implements Serializable {
    public String Dipendente;
    public Date DataInizio;
    public Date FineRapportoEff;
    public int StatoContratto;
    public int IdContrattoDipendente;
    public String IdMansioni;
    public Integer IdCentroDiCosto;
    public String CentroDiCosto;
    public String NumeroContratto;
    public String CodFiscale;
    public Date DataNascita;
    public String ComuneNascita;
    public int FIDDContratto;
    public String ComuneDomicilio;
    public String Cellulare;
    public int IdAgente;
    public int StatoIcona;
    public String DescrizioneIcona;
    public int NrProroghe;
    public Integer VoucherAllocati;
    public Integer VoucherAssegnati;
    public Integer VoucherLiquidati;
    public String FIDDContrattoEnc;
    public int ContrattoTipo;
    public String ContrattoTipoDesc;
    public int IdAziendaUtilizzatrice;
    public String AziendaUtilizzatrice;
    public String LabelContratto;
}
