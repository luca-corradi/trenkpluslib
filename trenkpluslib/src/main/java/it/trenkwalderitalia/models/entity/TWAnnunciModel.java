package it.trenkwalderitalia.models.entity;

import java.io.Serializable;
import java.util.Date;

public class TWAnnunciModel implements Serializable
{
    public int IDAnnunci;
    public Date DataDal;
    public Date DataAl;
    public int IDTipoSelezione;
    public int IDAgenteRiferimento;
    public int IDAzienda;
    public int IDSettoreAzienda;
    public String IntroduzioneAnnuncio;
    public String Introduzione1;
    public int IDFiliale;
    public String Introduzione2;
    public String Posizione;
    public String Annuncio;
    public int NumeroLavoratori;
    public int IDLivelloIstruzione;
    public String Contatti;
    public Date DateIns;
    public int IDUserIns;
    public Date DateUpd;
    public int IDUserUpd;
    public String Azienda;
    public boolean Visible;
    public int IDQualificaProFISTAT;
    public String QualificaProFISTAT;
    public String FilialeNome;
    public int IdMansione;
    public int StatoComunicazione;
    public int StatoComunicazioneHelpLavoro;
    public int StatoComunicazioneMonster;
    public boolean IsAnnuncioTEST;
    public String InformativaPrivacyText;
    public String InformativaPrivacyLink;
    public String LuoghiLavoro;
}

