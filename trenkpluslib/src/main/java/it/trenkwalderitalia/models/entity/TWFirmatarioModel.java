package it.trenkwalderitalia.models.entity;

import java.io.Serializable;

/**
 * Created by corradiluc on 14/01/2016.
 */
public class TWFirmatarioModel  implements Serializable {
    public int IdAgenteFirmatario;
    public String AgenteFirmatario;
    public int VerificaFirmaPositivo;
    public int VerificaFirmaNegativo;
    public int StatoAgenteDisplayCard;
    public boolean IsFirmaRemote;
    public boolean IsAutorizzatoAnnullamento;
    public int LivelloAutorizzazione;
}
