package it.trenkwalderitalia.models.entity;

import java.io.Serializable;
import java.util.Date;

public class TWMeetingModel implements Serializable {
    public int IdAgenteMeeting;
    public int IdAgenteTarget;
    public String AgenteTarget;
    public boolean IsOnlineAgenteTarget;
    public int IdAgenteOwner;
    public String AgenteOwner;
    public boolean IsOnlineAgenteOwner;
    public String Posizione;
    public String Titolo;
    public String Descrizione;
    public String FileKey;
    public int IdUserIns;
    public Date DateIns;
    public int IdApplicationIns;
    public int IdUserUpd;
    public Date DateUpd;
    public int IdApplicationUpd;
    public int IdCmsAnnunci;
    public Date DataInizio;
    public Date DataFine;
    public String IdAgenteMeetingEnc;
}
