package it.trenkwalderitalia.models.entity;

public class TWProvinciaModel {
    public int IDProvincia;
    public String PVSigla;
    public String PVNome;
    public int IDRegione;
    public String Regione;
}
