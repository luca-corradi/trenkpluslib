package it.trenkwalderitalia.models.entity;


import java.io.Serializable;

public class TWWorkflowStarterFirmaModel implements Serializable{
    public int NumeroTentativiRecupero;
    public boolean IsDocumentoCreato;
    public String Messaggio;
    public String FIDDEnc;
}
