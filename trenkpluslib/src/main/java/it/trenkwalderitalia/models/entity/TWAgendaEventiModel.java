package it.trenkwalderitalia.models.entity;

/**
 * Created by moncigoale on 03/03/2016.
 */
public class TWAgendaEventiModel {
    public int IDAgenteMeeting;
    public String DataInizio;
    public String DataFine;
    public boolean IsVisibleDipendente;
    public String TooltipText;
    public String NumeroDaRichiamare;
    public boolean IsConfermato;
    public String Fidd;
    public String title;
    public String Descrizione;
}
