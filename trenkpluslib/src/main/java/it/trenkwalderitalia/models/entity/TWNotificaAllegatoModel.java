package it.trenkwalderitalia.models.entity;


public class TWNotificaAllegatoModel {

        /**  id messaggio critico allegati */
        public int IdMessaggiCriticiAllegati;

        /**  id della notifica */
        public int IdMessaggiCritici;

        /**  Nome dell'allegato */
        public String Nome;

        /**  Descrizione */
        public String Descrizione;

        /**  Dimensione */
        public long FileSize;

        /**  Durata */
        public long FileDuration;

        /**  Formato */
        public String MimeType;

        /**  Indica se è obbligatorio */
        public boolean Obbligatorio;

        /**  Versione */
        public String Versione;

        /**  Thumbnail */
        public String Thunbnail;

        /**  id della categoria */
        public int IdCategoria;

        /**  Descrizione della categoria */
        public String Categoria;
    }
