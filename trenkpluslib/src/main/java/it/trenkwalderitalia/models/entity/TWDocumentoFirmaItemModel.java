package it.trenkwalderitalia.models.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by corradiluc on 14/01/2016.
 */
public class TWDocumentoFirmaItemModel implements Serializable {


    public int IdFirmaDigitaleImage;
    public String Firmatario;
    public int Stato;
    public int IdAgente;
    public int IdAgenteFirmatario;
    public Date DateTimeFirma;
    public int IndiceFirma;
    public int TipoFirma;
    public int VerificaFirmaPositivo;
    public int VerificaFirmaNegativo;
    public int LivelloAutorizzazione;
    public String Nota;
    public boolean IsFirmaRemote;
    public boolean IsAutorizzatoAnnullamento;
    public boolean IsFirmaScaduta;
    public boolean IsFirmaTradizionale;
    public Date DataScadenzaFirma;
    public Date DataInserimentoFirma;
    public int StatoAgenteDisplayCard;
    public List<TWFirmatarioModel> FirmatariAbilitati;
    //public FirmaDocumentoRichiestaModel SignRequestData;
}
