package it.trenkwalderitalia.models.entity;

import java.io.Serializable;
import java.util.Date;

public class TWArchivioDocumentoModel implements Serializable {
    public int IDDipendente;
    public int IdAgente;
    public int TipoDoc;
    public String Descrizione;
    public int NumeroDocumenti;
    public String Bookmark;
    public int FIDD;
    public Date DataCompetenza;

    public String FIDDEnc;
}
