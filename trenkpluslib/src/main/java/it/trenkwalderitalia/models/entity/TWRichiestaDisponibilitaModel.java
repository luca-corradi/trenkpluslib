package it.trenkwalderitalia.models.entity;

import java.io.Serializable;
import java.util.Date;

import it.trenkwalderitalia.models.util.ListGroup;

public class TWRichiestaDisponibilitaModel implements ListGroup {
    public static final int RDC_STATO_ATTESA = 0;
    public static final int RDC_STATO_ACCETTATO = 1;
    public static final int RDC_STATO_RIFIUTATO = 2;

    public int IdAziendaCollaboratori;
    public Date ScadenzaRichiesta;
    public int Stato;
    public String RagioneSociale;
    public String Dipendente;
    public int IdAzienda;
    public int IdDipendente;
    public String TestoNotifica;
    public int IdAziendaCollaboratoriRichiestaDisponibilita;

    public int IdAziendaCollaboratoriCampagna;
    public boolean NotificaInviata;
    public Date DataInvioNotifica;
    public String TitoloCampagna;
    public int IdComune;
    public String Comune;
    public String Indirizzo;
    public String Frazione;
    public int NumeroLavoratori;
    public String Luogo;

    //region Header
    public boolean isHeader;
    public String header;

    public static TWRichiestaDisponibilitaModel newHeader(String header){
        TWRichiestaDisponibilitaModel instance = new TWRichiestaDisponibilitaModel();
        instance.header = header;
        instance.isHeader = true;
        return instance;
    }

    @Override
    public boolean isHeader() {
        return isHeader;
    }

    @Override
    public String getHeader() {
        return header;
    }
    //endregion
}
