package it.trenkwalderitalia.models.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by corradiluc on 14/01/2016.
 */
public class TWDocumentoAccessoModel implements Serializable {
    public String Utente;
    public Date AccessTime;
    public String ModoDiAccesso;
}
