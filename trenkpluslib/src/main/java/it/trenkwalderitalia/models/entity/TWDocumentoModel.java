package it.trenkwalderitalia.models.entity;

import java.io.Serializable;
import java.util.List;

public class TWDocumentoModel implements Serializable {
    public String FIDDEnc;
    //public List<Lotto> Lotti;
    public TWFirmaWorkflowModel FirmaDigitaleWorkFlow;
    public List<TWDocumentoAccessoModel> AccessiDocumento;
    public List<TWDocumentoSummaryItemModel> Summaries;
    public TWDocumentoFirmeModel Firma;
}
