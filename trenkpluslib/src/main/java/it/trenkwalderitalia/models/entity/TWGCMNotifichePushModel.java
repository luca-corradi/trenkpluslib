package it.trenkwalderitalia.models.entity;

import java.io.Serializable;


public class TWGCMNotifichePushModel implements Serializable {

    public String titolo;
    public String masterRecordType; //DOC, DIP, RDC
    public String idMasterRecord;
    //public int type = 1;
    public String message;
    public String info;
    public String fiddEnc;

    public String toString(){
        return titolo + ": " + message + " (" + masterRecordType + " )";
    }
}
