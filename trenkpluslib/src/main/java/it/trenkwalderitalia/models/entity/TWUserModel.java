package it.trenkwalderitalia.models.entity;

import java.io.Serializable;

public class TWUserModel implements Serializable {

    public String username;
    public String password;
    public int idAgenteSmart;
    public int idAgente;
    public String idAgenteEnc;

    public String CognomeNome;
}
