package it.trenkwalderitalia.models.entity;

import java.io.Serializable;
import java.util.Date;

import it.trenkwalderitalia.trenkpluslib.TrenkPlusClient;

public class TWNewsModel implements Serializable {
    public int IDNews;
    public Date DataPubblicazioneDal;
    public Date DataPubblicazioneAl;
    public String Titolo;
    public String Testo;
    public int IdNewsCategoria;
    public String NewsCategoria;
    public boolean InEvidenza;
    public boolean Pubblica;
    public String PubblicaNews;
    public Date DateIns;
    public int IDUserIns;
    public Date DateUpd;
    public int IDUserUpd;
    public Date DatePublic;
    public int IDUserPublic;
    public boolean SezioneHome;
    public boolean SezioneCliente;
    public boolean SezioneDipendente;
    public String PathImmagineHome;
    public String PathImmagineInterna;
    public int IdCMSUpload;
    public String IdCMSUploadEnc;

    public String getImageUri(){
        return String.format("%s/File/%s", TrenkPlusClient.URL_TRENKWALDERITALIA, IdCMSUploadEnc);
    }
}
