package it.trenkwalderitalia.models.entity;

public class TWNotificaTipoModel {
    /**  Id notifiche tipo */
    public int IdNotificheTipo;

    /**  Descrizione della notifica */
    public String NotificheTipo;
}
