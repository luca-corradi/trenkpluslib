package it.trenkwalderitalia.models.entity;

import java.util.Date;

import it.trenkwalderitalia.models.util.ListGroup;

public class TWCandidaturaModel implements ListGroup {
    public int IdCmsAnnunciProfiliCandidati;
    public int IdCmsAnnunci;
    public int TipoCandidatura;
    public int StatoCandidato;
    public int StatoAzienda;
    public int StatoProfilo;
    public int Rating;
    public Date DataDal;
    public Date DataAl;
    public boolean RichiestaDisponibilitaInviata;
    public String Posizione;
    public String FilialeRif;
    public String Annuncio;
    public boolean IsChiusura;

    //region Header
    public boolean isHeader;
    public String header;

    public static TWCandidaturaModel newHeader(String header){
        TWCandidaturaModel instance = new TWCandidaturaModel();
        instance.header = header;
        instance.isHeader = true;
        return instance;
    }

    @Override
    public boolean isHeader() {
        return isHeader;
    }

    @Override
    public String getHeader() {
        return header;
    }
    //endregion
}
