package it.trenkwalderitalia.models.entity;

import java.io.Serializable;
import java.util.Date;

public class TWDocumentoDaFirmareModel implements Serializable {
    public String Tag;
    public String LinkLabel;
    public String FIDD;
    public int IdFiliale;
    public Date DateIns;
    public int IdAgente;
    public String Agente;
    public String Filiale;
    public int RiferimentoMasterWorkflow;
    public int IdFirmaDigitaleWorkflow;
    public int IdUnitaOrganizzativa;
    public int DocumentoIsFirmato;
    public Date DataFirmaDocumento;
    public int StatoWorkflow;
    public String FIDDEnc;
}
