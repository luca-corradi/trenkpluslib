package it.trenkwalderitalia.models.entity;

import java.io.Serializable;
import java.util.Date;

public class TWMeetingSettingModel implements Serializable {
    public int IdAgenteMeeting;
    public int IdAgente;
    public int LivelloAutorizzazione;
    public String RoomID;
    public String ExtensionID;
    public String ServerHttpUrl;
    public String ServerHttpsUrl;
    public String LicodeGitBranch;
    public int TipoAgente;
    public int TipoMeeting;
    public String TipoMeetingLabel;
    public String Ruolo;
    public String Nome;
    public boolean AudioEnabled;
    public boolean VideoEnabled;
    public boolean VideoSharingEnabled;
    public boolean RegistrazioneEnabled;
    public int IdAgenteProtagonista;
    public Date DataInizio;
    public String AgenteTag;
    public String Titolo;
    public String Descrizione;
    public String ElencoPartecipanti;
    public int CountPartecipanti;
    public boolean MaiEntrato;
    public String IdAgenteMeetingEnc;
}
