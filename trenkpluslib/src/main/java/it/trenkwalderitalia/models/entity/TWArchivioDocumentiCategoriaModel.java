package it.trenkwalderitalia.models.entity;

import java.io.Serializable;

public class TWArchivioDocumentiCategoriaModel implements Serializable {
    public int TipoDoc;
    public String Descrizione;
    public int NumeroDocumenti;
}
