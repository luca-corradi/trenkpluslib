package it.trenkwalderitalia.models.entity;

import java.io.Serializable;

/**
 * Created by corradiluc on 14/01/2016.
 */
public class TWDocumentoSummaryItemModel implements Serializable {
    public String Label;
    public String Testo;
}
