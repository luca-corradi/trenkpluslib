package it.trenkwalderitalia.models.entity;


import java.io.Serializable;

public class TWFirmaDocumentoStarterModel implements Serializable {
    public String IdFirmaDigitaleWorkflowStarterFirmeEnc;
    public int IdFirmaDigitaleWorkflowStarterFirme;
    public int IdFirmaDigitaleWorkflowSnapshot;
    public boolean OnError;
    public String TransazioneIreth;
    public String Messaggio;
    public String MessaggioDebug;
}
