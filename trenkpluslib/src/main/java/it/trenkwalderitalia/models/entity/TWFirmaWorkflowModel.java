package it.trenkwalderitalia.models.entity;

import java.io.Serializable;

/**
 * Created by corradiluc on 14/01/2016.
 */
public class TWFirmaWorkflowModel implements Serializable {
    public int IdFirmaDigitaleWorkFlow;
    public String RiferimentoMasterWorkFlow;
    public int DigitalCertificate;
    public int PageCount;
    public int StatoFirma;
}
