package it.trenkwalderitalia.models.entity;

import java.io.Serializable;
import java.util.List;

public class TWNotificaModel implements Serializable {
    public int _ID;

    /**  id del messaggio */
    public int IdMessaggio;

    /**  messaggio */
    public String Messaggio;

    /**  priorità */
    public int Priorita;

    /**  fidd */
    public String FIDD;

    /**  Tipo di notifica */
    public String TipoNotifica;

    /**  Descrizione tipo di notifica */
    public String DescrizioneTipoNotifica;

    /**  id del tipo di notifica */
    public int IdTipoNotifica;

    /**  id dell0agente mittente */
    public int IdAgenteMittente;

    /**  Nome dell'agente mittente */
    public String AgenteMittente;

    /**  Data di inserimento */
    public String DataInserimento;

    /**  indica se la notifica è da leggere */
    public boolean DaLeggere;

    /**  Data di lettura */
    public String DataLettura;

    /**  Eventuale url di dettaglio */
    public String UrlDetail;

    /**  Eventuale url di dettaglio per trenkplus */
    public String TRKPUrlDetail;

    /**  Eventuali allegati */
    public List<TWNotificaAllegatoModel> Allegati;

    /** Sezione notifica */
    public int IdMasterRecord;

    /** Sezione notifica criptata */
    public String IdMasterRecordEnc;

}
