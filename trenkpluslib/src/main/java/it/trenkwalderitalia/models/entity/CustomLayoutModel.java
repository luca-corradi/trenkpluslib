package it.trenkwalderitalia.models.entity;

/**
 * Created by moncigoale on 29/03/2016.
 */
public class CustomLayoutModel {

    public boolean CustomLayoutEnable;
    public int IdCMSUploadTipologia;
    public int IdAzienda;
    public String NomeFileLogo;
    public String CryptLogo;
    public String UrlLogo;
    public String RagioneSociale;       //Nome Azienda

}
