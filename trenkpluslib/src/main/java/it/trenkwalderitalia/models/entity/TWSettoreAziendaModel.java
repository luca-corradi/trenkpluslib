package it.trenkwalderitalia.models.entity;

public class TWSettoreAziendaModel {
    public int IdMansione;
    public String Mansione;
    public int Livello;
    public int ParentId;
    public boolean Visibile;
}
