package it.trenkwalderitalia.models.entity;

import java.io.Serializable;

public class TWWorkflowStarterModel implements Serializable {
    public String IdFirmaDigitaleWorkflowCript;
    public int NumeroTentativiRecupero;
    public boolean IsDocumentoCreato;
    public String Messaggio;
    public String MessaggioDebug;
    public String FIDDEnc;
}
