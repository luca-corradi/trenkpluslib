package it.trenkwalderitalia.models.request;

public class TWRichiestaTimbraCartellinoModel {
    public int Tipo;
    public int IdAzienda;
    public int IdTotem;
    public String Coordinate;
}
