package it.trenkwalderitalia.models.request;

import it.trenkwalderitalia.models.TWRispostaBaseModel;
import it.trenkwalderitalia.models.entity.TWDocumentoModel;

/**
 * Created by corradiluc on 14/01/2016.
 */
public class TWRispostaDocumentoModel extends TWRispostaBaseModel {

    public TWRispostaDocumentoModel(TWRispostaBaseModel model){
        super(model);
    }

    public TWDocumentoModel Dati;
}
