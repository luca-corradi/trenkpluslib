package it.trenkwalderitalia.models.request;

import java.util.Date;

/**
 * Created by moncigoale on 03/03/2016.
 */
public class TWRichiestaAgendaEventiModel {
    public Date Dal;
    public Date Al;
}
