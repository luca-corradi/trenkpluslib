package it.trenkwalderitalia.models.request;

import java.util.Date;

public class TWRichiestaNewsModel {
    public Date Periodo;
    public boolean Pubblica;
    public boolean InEvidenza;
}
