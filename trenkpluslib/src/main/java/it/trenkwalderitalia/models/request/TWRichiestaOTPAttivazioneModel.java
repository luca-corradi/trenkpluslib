package it.trenkwalderitalia.models.request;

public class TWRichiestaOTPAttivazioneModel {
    public String CodiceAttivazione;
    public String Pin;
    public int VersioneSP;
}
