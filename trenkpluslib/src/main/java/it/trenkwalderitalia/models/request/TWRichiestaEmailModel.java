package it.trenkwalderitalia.models.request;

import java.util.List;

public class TWRichiestaEmailModel {
    public int IdAgente;
    public List<String> To;
    public List<String> Cc;
    public List<String> Ccn;
    public String From;
    public String Subject;
    public List<String> Attachments;
    public String Body;
    public String FiddEnc;
}
