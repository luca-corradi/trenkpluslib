package it.trenkwalderitalia.models.request;

public class TWRichiestaAggiornaTokenModel {
    /**  id agente smart app */
    public int IdAgenteSmartApp;

    /**  id del device */
    public String DeviceId;

    /**  id dell'agente */
    public int IdAgente;

    /**  Token */
    public String Token;

    /**  Versione del sistema operativo */
    public String VersioneOS;

    /**  Versione dell'applicazione */
    public String VersioneApp;
}
