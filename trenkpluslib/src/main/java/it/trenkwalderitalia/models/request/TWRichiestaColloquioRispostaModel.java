package it.trenkwalderitalia.models.request;

/**
 * Created by moncigoale on 24/03/2016.
 */
public class TWRichiestaColloquioRispostaModel {
    public int IdRichiestaColloquio;
    public int StatoProposta;
    public String Testo;
}
