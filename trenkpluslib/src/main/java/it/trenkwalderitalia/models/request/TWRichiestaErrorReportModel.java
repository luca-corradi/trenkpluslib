package it.trenkwalderitalia.models.request;


import android.text.TextUtils;

import java.util.Map;

import it.trenkwalderitalia.trenkpluslib.utils.ExceptionUtils;

public class TWRichiestaErrorReportModel {

    public TWRichiestaErrorReportModel(){ }

    public TWRichiestaErrorReportModel(String message){
        this.message = message;
    }

    public TWRichiestaErrorReportModel(String message, String stacktrace, Map<String, Object> extraInfo){
        this.message = message;
        this.stacktrace = stacktrace;
        this.extraInformation = extraInfo;
    }

    public TWRichiestaErrorReportModel(Throwable throwable){
        this(throwable, null);
    }

    public TWRichiestaErrorReportModel(Throwable throwable, Map<String, Object> extraInfo){
        if(throwable != null){
            String msg = throwable.getMessage();
            this.message = TextUtils.isEmpty(msg) ? throwable.toString() : msg;
            this.stacktrace = ExceptionUtils.getStackTrace(throwable);
        }
        this.extraInformation = extraInfo;
    }

    public String message;
    public String stacktrace;
    public Map<String, Object> extraInformation;
}
