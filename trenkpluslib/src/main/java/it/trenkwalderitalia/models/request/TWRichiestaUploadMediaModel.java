package it.trenkwalderitalia.models.request;

import java.io.File;

import it.trenkwalderitalia.models.util.IgnoreMapping;


public class TWRichiestaUploadMediaModel {
    public int IDAgenteMultimedia;
    public int IDTipoMultimedia;
    public String Titolo;
    public String Descrizione;
    public boolean IsFotoCV;
    public boolean ClientSideMultimediaChecked;
    public int IdAgenteDipendente;
    public int IdDipendenteCurriculumRichiestaInformazioniIntegrative;

    @IgnoreMapping
    public File file;

    @IgnoreMapping
    public String mimeType;

    public void setFile(File file, String mimeType){
        this.file = file;
        this.mimeType = mimeType;
    }
}
