package it.trenkwalderitalia.models.request;

public class TWRichiestaRapportiLavoroModel {
    public Integer IdAzienda;
    public Integer TipologiaContrattuale;
    public Integer TipoSelezione;
    public Integer IdUnitaOrganizzativa;
    public String Periodo;
}
