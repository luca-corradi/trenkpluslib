package it.trenkwalderitalia.models.request;

import java.util.Date;

public class TWRichiestaNotificheModel
{
    /**  id della filiale */
    public int IdFiliale;

    /**  id del messaggio da cui inziare a recuperare le notifiche. 0 se tutte */
    public int IdMessaggio;

    /**  Tipo della notifica */
    public Integer IdTipo;

    /**  Se null ritorna tutte le notifiche, altrimenti se false torna solo quelle da leggere altrimenti solo quelle lette */
    public Boolean Confermate;

    /**  Filtro di ricerca nel contenuto */
    public String SearchContenuto;

    /**  Data di invio di origine */
    public Date DataInvioDal;

    /**  Data di invio di fine */
    public Date DataInvioAl;

    /**  Numero massimo di notifiche da recuperare */
    public int NumeroMaxNotifiche;

    /**  Indica se occorre caricare il tipo delle notifiche */
    public Boolean TipoNotificheToLoad;
}

