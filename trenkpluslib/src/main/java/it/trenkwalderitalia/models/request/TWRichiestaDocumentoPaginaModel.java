package it.trenkwalderitalia.models.request;

/**
 * Created by corradiluc on 14/01/2016.
 */
public class TWRichiestaDocumentoPaginaModel {
    /**  FIDD criptato */
    public String FIDDEnc;

    /**  Numero di pagina da renderizzare */
    public int PageNumber;
    public int Width;
    public int Heigth;
    public int Resolution;
    public double Scale;
    public int RotateAngle;
}
