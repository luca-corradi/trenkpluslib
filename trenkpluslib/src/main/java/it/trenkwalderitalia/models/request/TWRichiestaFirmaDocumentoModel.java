package it.trenkwalderitalia.models.request;

public class TWRichiestaFirmaDocumentoModel {
    public int IdFirmaDigitaleWorkFlow;
    public int IdAgente;
    public int IdAgenteFirmatario;
    public int FIDD;
    public String FIDDEnc;
    public int IndiceFirma;
    public String SessionId;
    public int IdApplication;
    public int IdTotem;

    /**  Token generato dalla security card oppure "RAPIDA" se Approvato è false */
    public String TokenPassCode;
    public String IpClient;
    public String Nota;
    public boolean Approvato;
    public int VerificaFirmaPositivo;
    public int VerificaFirmaNegativo;

    /**  Nome del servicepoint dove si sta eseguendo al firma */
    public String CanonicalName;

    /**  Il timestamp del client al momento dell'invio della richiesta */
    public String ClientTimestamp;
}
