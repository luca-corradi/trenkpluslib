package it.trenkwalderitalia.models.request;

import java.util.Date;

public class TWRichiestaRegistraNuovoUtenteModel
{
    public String CodiceFiscale;
    public String Cognome;
    public String Nome;
    public String Email;
    public String Cellulare;
    public int Sesso;
    public Date DataNascita;
    public int IdComuneNascita;
    public int IdComuneResidenza;
    public int IdComuneDomicilio;
    public int IdNazione;
    public String IPAddress;
}

