package it.trenkwalderitalia.models.request;

/**
 * Created by moncigoale on 12/01/2016.
 */
public class TWRichiestaAnnunciModel
{
    public int idAnnuncio;
    public String regione;
    public String provincia;
    public String mansione;
    public int take;
    public int nrRecord;
    public int idMansione;
    public int idAnnuncioRicerca;

}