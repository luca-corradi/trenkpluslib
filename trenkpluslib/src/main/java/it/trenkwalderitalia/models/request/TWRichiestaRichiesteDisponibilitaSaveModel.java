package it.trenkwalderitalia.models.request;


public class TWRichiestaRichiesteDisponibilitaSaveModel {
    public int IdAziendaCollaboratoriRichiestaDisponibilita;

    /**
     * 1 = Disponibile
     * 2 = Non disponibile
     */
    public int Stato;
}
